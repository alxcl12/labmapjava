package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.MultipleUsersException;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.Graph;
import socialnetwork.utils.Observable;
import socialnetwork.utils.Observer;
import socialnetwork.utils.Vertex;


import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * CRUD Controller for the app
 * Performs all operations
 */
public class Service implements Observable {
    //repository for users
    private PagingRepository<Long, User> userRepo;

    //repository for friendships
    private Repository<Tuple<Long, Long>, Friendship> friendshipRepo;

    //repository for friend requests
    private PagingRepository<Tuple<Long, Long>, FriendRequest> friendRequestRepo;

    //repository for messages
    private PagingRepository<Long, Message> messageRepo;

    //repository for events
    private PagingRepository<Long, Event> eventRepo;

    //graph for friendships
    private Graph<Long> friendshipGraph;

    public Service(PagingRepository<Long, User> userRepo, Repository<Tuple<Long,Long>, Friendship> friendshipRep,
                   PagingRepository<Tuple<Long, Long>, FriendRequest> friendRequestRepository,
                   PagingRepository<Long, Message> messageRepository,
                   PagingRepository<Long, Event> eventRepo) {

        this.friendshipRepo = friendshipRep;
        this.userRepo = userRepo;
        this.friendRequestRepo = friendRequestRepository;
        this.messageRepo = messageRepository;
        this.eventRepo = eventRepo;
        this.friendshipGraph = new Graph<>();
        this.buildGraph();
    }

    /**
     * Assigns an ID to an User
     * @return ID of user(Long)
     */
    private Long assignUserID(){
        Long id = null;
        try {
            do{
                id = User.generateID();
            }while (userRepo.findOne(id).isPresent());
        }
        catch (IllegalArgumentException e){
            assignUserID();
        }

        return id;
    }

    /**
     * Assigns an ID to a Message
     * @return ID of Message(Long)
     */
    private Long assignMessageID(){
        Long id = null;
        try {
            do{
                id = Message.generateID();
            }while (messageRepo.findOne(id).isPresent());
        }
        catch (IllegalArgumentException e){
            assignMessageID();
        }
        return id;
    }

    /**
     * Assigns an ID to an Event
     * @return ID of Event(Long)
     */
    private Long assignEventID(){
        Long id = null;
        try {
            do{
                id = Event.generateID();
            }while (eventRepo.findOne(id).isPresent());
        }
        catch (IllegalArgumentException e){
            assignMessageID();
        }
        return id;
    }

    /**
     * Builds friendship graph at startup based on existing friendships
     */
    private void buildGraph(){
        friendshipRepo.findAll().forEach(x->{
            friendshipGraph.addVertex(x.getId().getLeft());
            friendshipGraph.addVertex(x.getId().getRight());
            friendshipGraph.addEdge(x.getId().getLeft(), x.getId().getRight());
        });
    }

    /**
     * Adds user to the user repository
     * @param firstName of user
     * @param lastName of user
     * @throws ValidationException if user already exists
     * @throws ServiceException if repository throws IllegalArgumentException
     */
    public void addUser(String firstName, String lastName, String email, String password) {
        User toAdd = new User(firstName, lastName, email, password);
        Long id = this.assignUserID();
        toAdd.setId(id);

        Optional<User> e;

        try {
            e = userRepo.save(toAdd);
        }
        catch (IllegalArgumentException ex){
            throw new ServiceException("Unexpected error!\n");
        }

        if(e.isPresent()){
            throw new ValidationException("User already exists!\n");
        }
    }

    /**
     * Removes single user from user repository
     * @param firstName first name of user to be deleted
     * @param lastName last name of user to be deleted
     * @throws ValidationException if user does not exist
     * @throws ServiceException if repository throws IllegalArgumentException
     * @throws MultipleUsersException if there are multiple users with given first name and last name
     */
    public void removeUser(String firstName, String lastName){
        ArrayList<User> users = (ArrayList<User>) this.getAllByName(firstName, lastName);

        if(users.size() == 1){
            try {
                Optional<User> usr = userRepo.delete(users.get(0).getId());
                usr.ifPresent(user -> friendshipGraph.removeVertex(user.getId()));
            }
            catch (IllegalArgumentException e){
                throw new ServiceException("Unexpected error!\n");
            }
        }
        else if(users.size() == 0){
            throw new ValidationException("User does not exist to be deleted!\n");
        }
        else{
            throw new MultipleUsersException();
        }
    }

    /**
     * Removes an user from repository with given ID
     * @param ID ID of user to be deleted
     * @throws ServiceException if repository throws IllegalArgumentException
     */
    public void removeUserID(String ID){
        try {
            Optional<User> usr = userRepo.delete(Long.parseLong(ID));
            usr.ifPresent(user -> friendshipGraph.removeVertex(user.getId()));
        }
        catch (IllegalArgumentException e){
            throw new ServiceException("Unexpected error!\n");
        }
    }

    /**
     * Returns all users from user repository
     * @return Iterable User containing all users
     */
    public Iterable<User> getAllUsers(){
        return userRepo.findAll();
    }

    /**
     * Returns all users with given first name and last name
     * @param firstName first name of user
     * @param lastName last name of user
     * @return list of users
     */
    public Iterable<User> getAllByName(String firstName, String lastName){
        Iterable<User> allUsers = this.getAllUsers();

        ArrayList<User> toReturn = new ArrayList<>();

        for(User u: allUsers){
            if(u.getFirstName().equals(firstName) && u.getLastName().equals(lastName)){
                toReturn.add(u);
            }
        }

        return toReturn;
    }

    /**
     * Returns all friendships from friendship repository
     * @return Iterable Friendship containing all friendships
     */
    public Iterable<Friendship> getAllFriendship(){
        return friendshipRepo.findAll();
    }

    /**
     * Returns all friend requests from friend request repository
     * @return Iterable FriendRequest containing all friend requests
     */
    public Iterable<FriendRequest> getAllFriendRequests(){
        return friendRequestRepo.findAll();
    }

    /**
     * Returns friend request with given ID
     * @param id ID of friend request
     * @return friend request if it exists or null if it does not
     */
    public FriendRequest getFriendRequest(Tuple<Long, Long> id){
        List<FriendRequest> lst = StreamSupport.stream(friendRequestRepo.findAll().spliterator(), false)
                .filter(x-> x.getId().equals(id))
                .collect(Collectors.toList());

        if(lst.size() == 1){
            return lst.get(0);
        }
        else{
            return null;
        }
    }

    /**
     * Adds friendship to friendship repository
     * @param firstName1 first name of one user from friendship
     * @param lastName1 last name of second user from friendship
     * @param firstName2 first name of one user from friendship
     * @param lastName2 last name of second user from friendship
     * @throws ValidationException if friendship already exists
     * @throws ServiceException if repository throws IllegalArgumentException
     */
    public void addFriendship(String firstName1, String lastName1, String firstName2, String lastName2){
        ArrayList<User> user1 = (ArrayList<User>) this.getAllByName(firstName1, lastName1);
        ArrayList<User> user2 = (ArrayList<User>) this.getAllByName(firstName2, lastName2);

        if(user1.size() == 0 || user2.size() == 0){
            throw new ValidationException("User does not exist for friendship to be created!\n");
        }
        else if(user1.size() == 1 && user2.size() == 1){
            this.addFriendShipID(user1.get(0).getId().toString(), user2.get(0).getId().toString());
        }
        else {
            throw new MultipleUsersException();
        }
    }

    /**
     * Adds a friendship via the users ID
     * @param id1 first id of user
     * @param id2 second id of user
     * @throws ServiceException if repository throws IllegalArgumentException
     */
    public void addFriendShipID(String id1, String id2){
        Friendship friendship = new Friendship();

        Long longId1 = Long.parseLong(id1);
        Long longId2 = Long.parseLong(id2);

        Tuple<Long, Long> tuple1 = new Tuple<>(longId1, longId2);
        Tuple<Long, Long> tuple2 = new Tuple<>(longId2, longId1);

        try {
            if (friendshipRepo.findOne(tuple1).isPresent() || friendshipRepo.findOne(tuple2).isPresent()) {
                throw new ValidationException("Friendship already exists!\n");
            }
        }
        catch (IllegalArgumentException e){
            throw new ServiceException("Unexpected error!\n");
        }

        friendship.setId(tuple1);

        try {
            friendshipRepo.save(friendship);
        }
        catch (IllegalArgumentException e){
            throw new ServiceException("Unexpected error!\n");
        }

        friendshipGraph.addVertex(longId1);
        friendshipGraph.addVertex(longId2);

        friendshipGraph.addEdge(longId1, longId2);
        notifyObservers();
    }

    /**
     * Removes friendship from friendship repository
     * @param firstName1 first name of one user from friendship
     * @param lastName1 last name of second user from friendship
     * @param firstName2 first name of one user from friendship
     * @param lastName2 last name of second user from friendship
     * @throws ValidationException if friendship does not exist
     */
    public void removeFriendship(String firstName1, String lastName1, String firstName2, String lastName2){
        ArrayList<User> user1 = (ArrayList<User>) this.getAllByName(firstName1, lastName1);
        ArrayList<User> user2 = (ArrayList<User>) this.getAllByName(firstName2, lastName2);

        if(user1.size() == 0 || user2.size() == 0){
            throw new ValidationException("User does not exist for friendship to be deleted!\n");
        }
        else if(user1.size() == 1 && user2.size() == 1){
            Tuple<Long, Long> tuple1 = new Tuple<>(user1.get(0).getId(), user2.get(0).getId());
            Tuple<Long, Long> tuple2 = new Tuple<>(user2.get(0).getId(), user1.get(0).getId());

            Optional<Friendship> friendship1 = friendshipRepo.delete(tuple1);

            if(!friendship1.isPresent()){
                Optional<Friendship> friendship2 = friendshipRepo.delete(tuple2);

                if(!friendship2.isPresent()) {
                    throw new ValidationException("Friendship does not exist to be deleted!\n");
                }
            }

            friendshipGraph.removeEdge(user1.get(0).getId(), user2.get(0).getId());
            friendshipGraph.removeEdge(user2.get(0).getId(), user1.get(0).getId());
        }
        else {
            throw new MultipleUsersException();
        }
    }

    /**
     * Removes friendship by the ID
     * @param id1 first ID
     * @param id2 second ID
     */
    public void removeFriendshipID(String id1, String id2){
        Tuple<Long, Long> tuple1 = new Tuple<>(Long.parseLong(id1), Long.parseLong(id2));
        Tuple<Long, Long> tuple2 = new Tuple<>(Long.parseLong(id2), Long.parseLong(id1));

        Optional<Friendship> friendship1 = friendshipRepo.delete(tuple1);
        if(!friendship1.isPresent()){
            Optional<Friendship> friendship2 = friendshipRepo.delete(tuple2);

            if(!friendship2.isPresent()) {
                throw new ValidationException("Friendship does not exist to be deleted!\n");
            }
        }
        friendshipGraph.removeEdge(Long.parseLong(id1), Long.parseLong(id2));
        friendshipGraph.removeEdge(Long.parseLong(id2), Long.parseLong(id1));
        notifyObservers();
    }

    /**
     * Returns list of users which belong to the most sociable community
     * @return List of users
     * @deprecated
     */
    public ArrayList<User> getBiggestCommunity(){
        ArrayList<Long> ids = friendshipGraph.getMaxComponent();

        ArrayList<User> toReturn = new ArrayList<>();

        for(Long i: ids){
            Optional<User> u = userRepo.findOne(i);
            u.ifPresent(toReturn::add);
        }
        return toReturn;
    }

    /**
     * Returns list of users which belong to the most sociable community
     * @return List of users
     */
    public ArrayList<User> getMostSociable(){
        ArrayList<Vertex<Long>> ids = friendshipGraph.getLongestElementaryPath();

        ArrayList<User> toReturn = new ArrayList<>();

        for(Vertex<Long> i: ids){
            Optional<User> u = userRepo.findOne(i.getValue());
            u.ifPresent(toReturn::add);
        }
        return toReturn;
    }

    /**
     * Returns number of user communities
     * @return integer, number of communities
     */
    public int getNrCommunities(){
        return friendshipGraph.getNrConnectedComponents();
    }

    /**
     * Returns ArrayList containing FriendDTO with all friends of input user started in input month
     * @param firstName first name of user
     * @param lastName last name of user
     * @param month start month of friendship
     * @return ArrayList of FriendDTOs
     * @throws ValidationException if user does not exist
     * @throws MultipleUsersException if there are multiple users with given first name and last name
     */
    public Iterable<FriendDTO> getFriendsByMonth(String firstName, String lastName, Long month){
        ArrayList<User> users = (ArrayList<User>) this.getAllByName(firstName, lastName);

        if(users.size() == 1) {
            return this.getFriendsByMonthID(users.get(0).getId(), month);
        }
        else if(users.size() == 0){
            throw new ValidationException("User does not exist!\n");
        }
        else {
            throw new MultipleUsersException();
        }
    }

    /**
     * Returns ArrayList containing FriendDTO with all friends of input user by ID started in input month
     * @param UserID ID of user
     * @param month month of start for friendship
     * @return ArrayList of FriendDTOs
     */
    public Iterable<FriendDTO> getFriendsByMonthID(Long UserID, Long month){
        HashSet<FriendDTO> toReturn = new HashSet<>();

        Iterable<FriendDTO> initialGet = this.getFriendsID(UserID);
        StreamSupport.stream(initialGet.spliterator(), false)
                .filter(x -> x.getDate().getMonth().getValue() == month)
                .forEach(toReturn::add);

        return toReturn;
    }

    /**
     * Returns ArrayList containing FriendDTO with all friends of input user
     * @param firstName first name of user
     * @param lastName last name of user
     * @return ArrayList of FriendDTOs
     * @throws ValidationException if user does not exist
     * @throws MultipleUsersException if there are multiple users with given first name and last name
     */
    public Iterable<FriendDTO> getFriends(String firstName, String lastName){
        ArrayList<User> users = (ArrayList<User>) this.getAllByName(firstName, lastName);

        if(users.size() == 1) {
            return this.getFriendsID(users.get(0).getId());
        }
        else if(users.size() == 0){
            throw new ValidationException("User does not exist!\n");
        }
        else {
            throw new MultipleUsersException();
        }
    }

    /**
     * Returns Iterable of Users containing all users that are friends with user with currentID
     * @param currentID long, id of user
     * @return Iterable of users
     */
    public Iterable<User> getFriendsList(Long currentID){
        Iterable<Friendship> friendships = friendshipRepo.findAll();

        HashSet<User> toReturn = new HashSet<>();

        friendships.forEach(x->{
            if(x.getId().getLeft().equals(currentID)){
                toReturn.add(this.getUser(x.getId().getRight()));
            } else if (x.getId().getRight().equals(currentID)) {
                toReturn.add(this.getUser(x.getId().getLeft()));
            }
        });

        return toReturn;
    }

    /**
     * Returns ArrayList containing FriendDTO with all friends of input user by ID
     * @param UserID ID of user
     * @return ArrayList of FriendDTOs
     */
    public Iterable<FriendDTO> getFriendsID(Long UserID){
        Predicate<Friendship> friendRight = x-> x.getId().getRight().equals(UserID);
        Predicate<Friendship> friendLeft = x-> x.getId().getLeft().equals(UserID);
        Iterable<Friendship> friendships = friendshipRepo.findAll();

        HashSet<FriendDTO> toReturn = new HashSet<>();

        StreamSupport.stream(friendships.spliterator(), false)
                .filter(friendLeft)
                .forEach(x-> toReturn.add(new FriendDTO(userRepo.findOne(x.getId().getRight()).get().getFirstName(),
                        userRepo.findOne(x.getId().getRight()).get().getLastName(),
                        x.getDate())));

        StreamSupport.stream(friendships.spliterator(), false)
                .filter(friendRight)
                .forEach(x-> toReturn.add(new FriendDTO(userRepo.findOne(x.getId().getLeft()).get().getFirstName(),
                        userRepo.findOne(x.getId().getLeft()).get().getLastName(),
                        x.getDate())));

        return toReturn;
    }

    /**
     * Adds friend request to repository
     * @param firstName first name of receiver
     * @param lastName last name of receiver
     * @param currentID id of sender
     * @throws MultipleUsersException if there are multiple users with given first name and last name
     */
    public void addFriendRequest(String firstName, String lastName, Long currentID){
        ArrayList<User> users = (ArrayList<User>) this.getAllByName(firstName, lastName);

        if(users.size() == 0){
            throw new ValidationException("User does not exist for friend request to be sent!\n");
        }
        else if(users.size() == 1){
            this.addFriendRequestID(users.get(0).getId(), currentID);
        }
        else {
            throw new MultipleUsersException();
        }
    }

    /**
     * Adds friend request to repository via users ID
     * @param otherID id of receiver
     * @param currentID id of requester
     * @throws ValidationException if friend request already exists
     * @throws ServiceException if IllegalArgumentException is thrown by repository
     */
    public void addFriendRequestID(Long otherID, Long currentID){
        Tuple<Long, Long> tuple1 = new Tuple<>(currentID, otherID);
        Tuple<Long, Long> tuple2 = new Tuple<>(otherID, currentID);

        FriendRequest friendRequest = new FriendRequest("Pending", currentID, otherID);

        try {
            if(friendRequestRepo.findOne(tuple1).isPresent() || friendRequestRepo.findOne(tuple2).isPresent()){
                throw new ValidationException("Friend request already exists!\n");
            }
        }
        catch (IllegalArgumentException e){
            throw new ServiceException("Unexpected error!\n");
        }

        friendRequest.setId(tuple1);

        try {
            friendRequestRepo.save(friendRequest);
        }
        catch (IllegalArgumentException e){
            throw new ServiceException("Unexpected error!\n");
        }

        notifyObservers();
    }

    /**
     * Removes friend request by its ID
     * @param otherID id of one user from request
     * @param currentID id of other user from request
     */
    public void removeFriendRequestID(Long otherID, Long currentID){
        Tuple<Long, Long> tuple1 = new Tuple<>(currentID, otherID);
        Tuple<Long, Long> tuple2 = new Tuple<>(otherID, currentID);

        Optional<FriendRequest> req1 = friendRequestRepo.delete(tuple1);
        if(!req1.isPresent()){
            Optional<FriendRequest> req2 = friendRequestRepo.delete(tuple2);
            if(!req2.isPresent()){
                throw new ValidationException("Friend request does not exist to be deleted!\n");
            }
        }
        notifyObservers();
    }

    /**
     * Sends message to user with given first name and last name
     * @param currentID id of sender
     * @param message message to be sent
     * @throws ValidationException if user does not exist
     * @throws MultipleUsersException if there are multiple users with given first name and last name
     */
    public void sendMessage(List<Long> idList, Long currentID, String message){
        this.sendMessageID(idList, currentID, message);
    }

    /**
     * Sends message to user with given ID
     * @param otherID id of receiver
     * @param currentID id of sender
     * @param message message to be sent
     * @throws ValidationException if user does not exist
     */
    public void sendMessageID(List<Long> otherID, Long currentID, String message){
        User u1 = this.getUser(currentID);

        Long id = this.assignMessageID();

        if(u1 != null) {
            List<User> userList = new ArrayList<>();
            for(Long idF: otherID){
                User u2 = this.getUser(idF);
                userList.add(u2);
            }

            Message message1 = new Message(this.getUser(currentID), userList, message);
            message1.setId(id);

            try {
                messageRepo.save(message1);
                notifyObservers();
            } catch (IllegalArgumentException exc) {
                throw new ServiceException("Unexpected error!\n");
            }
        }
        else {
            throw new ValidationException("User does not exist for message to be sent!\n");
        }
    }

    /**
     * Returns Iterable containing all friend requests(pending) received by user with currentID
     * @param currentID id of user to receive requests
     * @return Iterable containing all pending friend requests
     */
    public Iterable<FriendRequest> getRequests(Long currentID){
        Iterable<FriendRequest> requests = friendRequestRepo.findAll();
        HashSet<FriendRequest> toReturn = new HashSet<>();

        StreamSupport.stream(requests.spliterator(), false)
                .filter(x-> x.getReceiverID().equals(currentID))
                .forEach(toReturn::add);

        return toReturn;
    }

    /**
     * Returns Iterable containing all users that are not friend with current user nor there are friend requests
     * @param currentID id of current user
     * @return Iterable of users
     */
    public Iterable<User> getAllNonFriends(Long currentID){
        HashSet<User> toReturn = new HashSet<>();
        Iterable<FriendRequest> requests = getAllFriendRequests();
        Iterable<User> friends = getFriendsList(currentID);

        List<User> friendList = StreamSupport.stream(friends.spliterator(), false)
                .collect(Collectors.toList());
        List<FriendRequest> friendReqList = StreamSupport.stream(requests.spliterator(), false)
                .collect(Collectors.toList());


        Iterable<User> all = getAllUsers();
        all.forEach(x->{
            if(!x.getId().equals(currentID)) {
                if (!friendList.contains(x)) {
                    int flag = 0;
                    for(FriendRequest req: friendReqList){
                        if((req.getRequesterID().equals(x.getId()) && req.getReceiverID().equals(currentID))
                        || (req.getRequesterID().equals(currentID) && req.getReceiverID().equals(x.getId())) ){
                            flag = -9;
                            break;
                        }
                    }
                    if(flag == 0){
                        toReturn.add(x);
                    }
                }
            }
        });

        return toReturn;
    }

    /**
     * Returns Iterable containing all messages received by user with currentID
     * @param currentID id of user to receive message
     * @return Iterable containing all messages
     */
    public Iterable<Message> getMessagesUser(Long currentID){
        Iterable<Message> messages = messageRepo.findAll();
        HashSet<Message> toReturn = new HashSet<>();

        StreamSupport.stream(messages.spliterator(), false)
                .forEach(x->{
                    for(User u: x.getTo()){
                        if (u.getId().equals(currentID)) {
                            toReturn.add(x);
                        }
                    }
                });

        return toReturn;
    }

    /**
     * Returns user from user repository
     * @param userID ID of user to be returned
     * @return User
     */
    public User getUser(Long userID){
        return userRepo.findOne(userID).get();
    }

    /**
     * Marks input friend request as accepted
     * @param req FriendRequest
     */
    public void acceptFriendRequest(FriendRequest req){
        req.setStatus("Approved");
        friendRequestRepo.update(req);
        notifyObservers();
    }

    /**
     * Marks input friend request as denied
     * @param req FriendRequest
     */
    public void denyFriendRequest(FriendRequest req){
        req.setStatus("Rejected");
        friendRequestRepo.update(req);
        notifyObservers();
    }

    /**
     * Sends reply message to an user
     * @param otherID id of receiver
     * @param currentID id of sender
     * @param message message to be sent
     * @param reply Message to be replayed
     */
    public void sendReply(List<Long> otherID, Long currentID, String message, Message reply){
        Long id = this.assignMessageID();

        User u1 = this.getUser(currentID);

        List<User> userList = new ArrayList<>();
        for(Long idF: otherID){
            User u2 = this.getUser(idF);
            userList.add(u2);
        }

        ReplyMessage replyMessage = new ReplyMessage(u1, userList, message, reply);
        replyMessage.setId(id);
        replyMessage.setDate(LocalDateTime.now());

        try {
            messageRepo.save(replyMessage);
        } catch (IllegalArgumentException exc) {
            throw new ServiceException("Unexpected error!\n");
        }
        notifyObservers();
    }

    /**
     * Returns list containing conversation between 2 users sorted by date
     * @param firstName1 first name of one user
     * @param lastName1 last name of one user
     * @param firstName2 first name of other user
     * @param lastName2 last name of other user
     * @return List of Message
     * @throws ValidationException if user does not exist
     * @throws MultipleUsersException if there are multiple users with given first names and last names
     */
    public List<Message> getMessagesUsers(String firstName1, String lastName1, String firstName2, String lastName2){
        ArrayList<User> user1 = (ArrayList<User>) this.getAllByName(firstName1, lastName1);
        ArrayList<User> user2 = (ArrayList<User>) this.getAllByName(firstName2, lastName2);

        if(user1.size() == 0 || user2.size() == 0){
            throw new ValidationException("User does not exist to show messages!\n");
        }
        else if(user1.size() == 1 && user2.size() == 1){
            return this.getMessagesUsersID(user1.get(0).getId(), user2.get(0).getId());
        }
        else {
            throw new MultipleUsersException();
        }
    }

    /**
     * Returns list containing conversation between 2 users sorted by date
     * @param id1 id of one user
     * @param id2 id of other user
     * @return List of Message
     */
    public List<Message> getMessagesUsersID(Long id1, Long id2){
        List<Message> toReturn = new ArrayList<>();

        Iterable<Message> all = messageRepo.findAll();

        StreamSupport.stream(all.spliterator(), false)
                .forEach(x->{
                    if(x.getFrom().getId().equals(id1)){
                        for(User u: x.getTo()){
                            if(u.getId().equals(id2)){
                                toReturn.add(x);
                                break;
                            }
                        }
                    }
                    if(x.getFrom().getId().equals(id2)){
                        for(User u: x.getTo()){
                            if(u.getId().equals(id1)){
                                toReturn.add(x);
                                break;
                            }
                        }
                    }
                });

        toReturn.sort(Comparator.comparing(Message::getDate));

        return toReturn;
    }

    /**
     * Returns list containing all messages received by user in interval
     * @param currentID ID of user to receive messages
     * @param start start date of interval
     * @param end end date of interval
     * @return List containing messages that match criteria
     */
    public List<Message> getMessagesPeriod(Long currentID, LocalDateTime start, LocalDateTime end){
        Iterable<Message> messages = messageRepo.findAll();
        ArrayList<Message> toReturn = new ArrayList<>();

        StreamSupport.stream(messages.spliterator(), false)
                .filter(x->x.getDate().isAfter(start) && x.getDate().isBefore(end))
                .forEach(x->{
                    for(User usr : x.getTo()){
                        if(usr.getId().equals(currentID)){
                            toReturn.add(x);
                            break;
                        }
                    }
                });

        return toReturn;
    }

    /**
     * Returns list containing friendships created by user in interval
     * @param currentID ID of current user
     * @param start start date of interval
     * @param end end date of interval
     * @return List containing friendships that match criteria
     */
    public List<Friendship> getFriendshipsPeriod(Long currentID, LocalDateTime start, LocalDateTime end){
        Iterable<Friendship> friendships = friendshipRepo.findAll();
        ArrayList<Friendship> toReturn = new ArrayList<>();

        StreamSupport.stream(friendships.spliterator(), false)
                .filter(x->x.getDate().isAfter(start) && x.getDate().isBefore(end))
                .forEach(x->{
                    if(x.getId().getLeft().equals(currentID) || x.getId().getRight().equals(currentID)){
                        toReturn.add(x);
                    }
                });

        return toReturn;
    }

    /**
     * Returns list containing messages received by one user from another in interval
     * @param currentID ID of user receiver
     * @param otherId ID of user sender
     * @param start start date of interval
     * @param end end date of interval
     * @return List containing messages that match criteria
     */
    public List<Message> getMessagesUserPeriod(Long currentID, Long otherId, LocalDateTime start, LocalDateTime end){
        Iterable<Message> messages = messageRepo.findAll();
        List<Message> toReturn = new ArrayList<>();

        StreamSupport.stream(messages.spliterator(), false)
                .filter(x->x.getDate().isBefore(end) && x.getDate().isAfter(start) && x.getFrom().getId().equals(otherId))
                .forEach(x->{
                    for(User usr: x.getTo()){
                        if(usr.getId().equals(currentID)){
                            toReturn.add(x);
                            break;
                        }
                    }
                });
        return toReturn;
    }

    /**
     * Adds Event to event repository
     * @param name name of event
     * @param description description of event
     * @param currentID id of event owner
     * @param date date of the event
     */
    public void addEvent(String name, String description, Long currentID, LocalDateTime date) {
        Event toAdd = new Event(getUser(currentID), date, name, description);
        Long id = this.assignEventID();
        toAdd.setId(id);

        try {
            eventRepo.save(toAdd);
        }
        catch (IllegalArgumentException ex){
            throw new ServiceException("Unexpected error!\n");
        }
        notifyObservers();
    }

    /**
     * Returns Iterable containing all events
     * @return Iterable of Events
     */
    public Iterable<Event> getAllEvents(){
        return eventRepo.findAll();
    }

    /**
     * Adds participant to an event
     * @param currentID ID of wannabe participant
     * @param eventID ID of event
     */
    public void addParticipant(Long currentID, Long eventID){
        Optional<Event> event = eventRepo.findOne(eventID);
        if(event.isPresent()){
            if(event.get().getParticipants() == null){
                event.get().setParticipants(new ArrayList<>());
            }

            List<User> participants = event.get().getParticipants();
            participants.add(userRepo.findOne(currentID).get());

            event.get().setParticipants(participants);

            eventRepo.update(event.get());

            notifyObservers();
        }
    }

    /**
     * Removes participant from event
     * @param currentID ID of participant to be removed
     * @param eventID ID of event
     */
    public void removeParticipant(Long currentID, Long eventID){
        Optional<Event> event = eventRepo.findOne(eventID);
        if(event.isPresent()){
            if(event.get().getParticipants() == null){
                throw new ValidationException("You need to follow in order to unfollow!");
            }

            List<User> participants = event.get().getParticipants();
            participants.remove(userRepo.findOne(currentID).get());

            event.get().setParticipants(participants);


            eventRepo.update(event.get());

            notifyObservers();
        }
    }

    //list containing all observers to the service
    private List<Observer> observers = new ArrayList<>();

    /**
     * Adds observer to this observable(service)
     * @param e Observer to be added
     */
    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    /**
     * Removes observer to this observable(service)
     * @param e Observer to be removed
     */
    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    /**
     * Notifies all observers to this observable
     */
    @Override
    public void notifyObservers() {
        observers.forEach(Observer::update);
    }

    //current page for non friends and size
    private int pageNonFriends = 0;
    private int sizeNonFriends = 1;

    public void setPageSizeNonFriends(int size){
        this.sizeNonFriends = size;
        userRepo.setPageSize(size);
    }

    public Set<User> getNextNonFriends(Long currentID){
        this.pageNonFriends++;
        return getNonFriendsOnPage(this.pageNonFriends, currentID);
    }

    public Set<User> getNonFriendsOnPage(int page, Long currentID){
        this.pageNonFriends = page;

        //with pageable
        //Pageable pageable = new PageableImplementation(page, this.sizeNonFriends);
        //Page<User> userPage = userRepo.findAll(pageable);

        //query limit offset database
        Iterable<User> userPage = userRepo.findOnPageNon(page, currentID);

        return (Set<User>) userPage;
    }

    //current page for friends and size
    private int pageFriends = 0;
    private int sizeFriends = 1;

    public void setPageSizeFriends(int size){
        this.sizeFriends = size;
        userRepo.setPageSize(size);
    }

    public Set<User> getNextFriends(Long currentID){
        this.pageFriends++;
        return getFriendsOnPage(this.pageFriends, currentID);
    }

    public Set<User> getFriendsOnPage(int page, Long currentID){
        this.pageFriends = page;

        //with pageable
//        Pageable pageable = new PageableImplementation(page, this.sizeFriends);
//        Page<User> userPage = userRepo.findAll(pageable);

        //with query limit offset
        Iterable<User> userPage = userRepo.findOnPageFriends(page, currentID);

        return (Set<User>) userPage;
    }

    //current page for requests and size
    private int pageReq = 0;
    private int sizeReq = 1;

    public void setPageSizeReq(int size){
        this.sizeReq = size;
        friendRequestRepo.setPageSize(size);
    }

    public Set<FriendRequest> getNextReq(Long currentID){
        this.pageReq++;
        return getReqOnPage(this.pageReq, currentID);
    }

    public Set<FriendRequest> getReqOnPage(int page, Long currentID){
        this.pageReq = page;

        //with pageable
//        Pageable pageable = new PageableImplementation(page, this.sizeReq);
//        Page<FriendRequest> reqPage = friendRequestRepo.findAll(pageable);

        //query offset limit db
        Iterable<FriendRequest> reqPage = friendRequestRepo.findOnPage(page);

        return StreamSupport.stream(reqPage.spliterator(), false)
                .filter(x->x.getReceiverID().equals(currentID) || x.getRequesterID().equals(currentID))
                .collect(Collectors.toSet());
    }

    //current page for messages(sent and received) and size
    private int pageMes = 0;
    private int pageMesSent = 0;
    private int sizeMes = 1;

    public void setPageSizeMes(int size){
        this.sizeMes = size;
        messageRepo.setPageSize(size);
    }

    public Set<Message> getNextMes(Long currentID){
        this.pageMes++;
        return getMesOnPage(this.pageMes, currentID);
    }

    public Set<Message> getNextMesSent(Long currentID){
        this.pageMesSent++;
        return getMesOnPageSent(this.pageMesSent, currentID);
    }

    public Set<Message> getMesOnPage(int page, Long currentID){
        this.pageMes = page;

        //with pageable
//        Pageable pageable = new PageableImplementation(page, this.sizeMes);
//        Page<Message> mesPage = messageRepo.findAll(pageable);

        Iterable<Message> mesPage = messageRepo.findOnPage(page);

        HashSet<Message> toReturn = new HashSet<>();
        StreamSupport.stream(mesPage.spliterator(), false)
                .forEach(x->{
                    for(User u: x.getTo()){
                        if(u.getId().equals(currentID)){
                            toReturn.add(x);
                            break;
                        }
                    }
                });

        return toReturn;
    }

    public Set<Message> getMesOnPageSent(int page, Long currentID){
        this.pageMesSent = page;

        //with pageable
//        Pageable pageable = new PageableImplementation(page, this.sizeMes);
//        Page<Message> mesPage = messageRepo.findAll(pageable);

        Iterable<Message> mesPage = messageRepo.findOnPage(page);

        HashSet<Message> toReturn = new HashSet<>();
        StreamSupport.stream(mesPage.spliterator(), false)
                .forEach(x->{
                    if(x.getFrom().getId().equals(currentID)) {
                        toReturn.add(x);
                    }
                });

        return toReturn;
    }

    /**
     * Populates with 20 users and befriends them with user A A
     * @throws FileNotFoundException If files containing names is not found
     */
    public void bigPopulateForA() throws FileNotFoundException {
        List<String> firstNames = new ArrayList<>();
        List<String> lastNames = new ArrayList<>();

        File firstNameFile = new File("C:\\Facultate\\New\\AnulII\\SemestrulIII\\Metodeavansatedeprogramare\\Laboratoare\\LabJava\\src\\main\\resources\\names\\firstnames.txt");
        File lastNameFile = new File("C:\\Facultate\\New\\AnulII\\SemestrulIII\\Metodeavansatedeprogramare\\Laboratoare\\LabJava\\src\\main\\resources\\names\\lastnames.txt");

        Scanner reader = new Scanner(firstNameFile);
        while(reader.hasNextLine()){
            String data = reader.nextLine();
            firstNames.add(data);
        }
        reader.close();

        reader = new Scanner(lastNameFile);
        while(reader.hasNextLine()){
            String data = reader.nextLine();
            lastNames.add(data);
        }
        reader.close();

        Random rand = new Random();
        for(int i=0; i<20; i++){
            String first = firstNames.get(rand.nextInt(firstNames.size()));
            String last = lastNames.get(rand.nextInt(lastNames.size()));

            User usr = new User(first, last, first.toLowerCase() + last.toLowerCase() + "@" + first.toLowerCase() + ".com", first.toLowerCase());
            Long id = this.assignUserID();
            usr.setId(id);

            userRepo.save(usr);

            Tuple<Long, Long> tuple1 = new Tuple<>(2901L, id);
            Tuple<Long, Long> tuple2 = new Tuple<>(id, 2901L);

            FriendRequest friendRequest = new FriendRequest("Pending", 2901L, id);
            friendRequest.setId(tuple1);

            friendRequestRepo.save(friendRequest);

            acceptFriendRequest(friendRequest);

            addFriendShipID("2901", id.toString());
        }
    }
}
