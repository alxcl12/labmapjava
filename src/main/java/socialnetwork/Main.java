package socialnetwork;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.database.FriendRequestDatabase;
import socialnetwork.repository.database.FriendshipDatabase;
import socialnetwork.repository.database.MessagesDatabase;
import socialnetwork.repository.database.UserDatabase;
import socialnetwork.service.Service;
import socialnetwork.ui.Console;
import socialnetwork.ui.GUI;

public class Main {
    public static void main(String[] args) {
//        Validator<User> userValidator = new UserValidator();
//        FriendshipValidator friendshipValidator = new FriendshipValidator();
//        FriendRequestValidator friendRequestValidator = new FriendRequestValidator();
//        MessageValidator messageValidator = new MessageValidator();
//
//
//        UserDatabase databaseUser = new UserDatabase(
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"),
//                userValidator);
//
//        FriendshipDatabase databaseFriendship = new FriendshipDatabase(
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"),
//                friendshipValidator);
//
//        FriendRequestDatabase databaseFriendRequest = new FriendRequestDatabase(
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"),
//                friendRequestValidator);
//
//        MessagesDatabase databaseMessages = new MessagesDatabase(
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
//                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"),
//                messageValidator);
//
//
//        Service service = new Service(databaseUser, databaseFriendship, databaseFriendRequest, databaseMessages);
//        Console cons = new Console(service);



        GUI.main(args);

//        try {
//            cons.run();
//        }
//        catch (IOException e){
//            System.out.println(e.getMessage());
//        }
    }
}


