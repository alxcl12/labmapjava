package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * Class represents friendship between 2 distinct users
 */
public class Friendship extends Entity<Tuple<Long,Long>> {
    LocalDateTime date; //date of friendship creation

    public Friendship() {
        date = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);
    }

    /**
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "id1=" + getId().getLeft() + " " +
                "id2=" + getId().getRight() + " " +
                "date=" + getDate() +
                '}';
    }
}
