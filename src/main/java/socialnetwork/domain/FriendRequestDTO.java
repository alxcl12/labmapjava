/*
 *  @author albua
 *  created on 22/11/2020
 */
package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Class used to transfer data about a friend request
 */
public class FriendRequestDTO {
    private String firstNameFriend;
    private String lastNameFriend;
    private LocalDateTime date;
    private String status;
    private Tuple<Long, Long> id;

    public FriendRequestDTO(String firstName, String lastName, LocalDateTime date, String status, Tuple<Long, Long> idReq){
        this.firstNameFriend = firstName;
        this.lastNameFriend = lastName;
        this.date = date;
        this.status = status;
        this.id = idReq;
    }

    public String getFirstNameFriend() {
        return firstNameFriend;
    }

    public void setFirstNameFriend(String firstNameFriend) {
        this.firstNameFriend = firstNameFriend;
    }

    public String getLastNameFriend() {
        return lastNameFriend;
    }

    public void setLastNameFriend(String lastNameFriend) {
        this.lastNameFriend = lastNameFriend;
    }

    public String getDate() {
        return date.format(Constants.DATE_TIME_FORMATTER);
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Tuple<Long, Long> getId() {
        return id;
    }

    public void setId(Tuple<Long, Long> id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendRequestDTO that = (FriendRequestDTO) o;
        return Objects.equals(firstNameFriend, that.firstNameFriend) &&
                Objects.equals(lastNameFriend, that.lastNameFriend) &&
                Objects.equals(date, that.date) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstNameFriend, lastNameFriend, date);
    }

    @Override
    public String toString() {
        return "FriendDTO{" +
                "firstNameFriend='" + firstNameFriend + '\'' +
                ", lastNameFriend='" + lastNameFriend + '\'' +
                ", date=" + date +
                '}';
    }
}
