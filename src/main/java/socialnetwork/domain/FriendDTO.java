/*
 *  @author albua
 *  created on 07/11/2020
 */
package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Class used to transfer data about a friend of user
 */
public class FriendDTO {
    private String firstNameFriend;
    private String lastNameFriend;
    private LocalDateTime date;

    public FriendDTO(String firstName, String lastName, LocalDateTime date){
        this.firstNameFriend = firstName;
        this.lastNameFriend = lastName;
        this.date = date;
    }

    public String getFirstNameFriend() {
        return firstNameFriend;
    }

    public void setFirstNameFriend(String firstNameFriend) {
        this.firstNameFriend = firstNameFriend;
    }

    public String getLastNameFriend() {
        return lastNameFriend;
    }

    public void setLastNameFriend(String lastNameFriend) {
        this.lastNameFriend = lastNameFriend;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendDTO friendDTO = (FriendDTO) o;
        return Objects.equals(firstNameFriend, friendDTO.firstNameFriend) &&
                Objects.equals(lastNameFriend, friendDTO.lastNameFriend) &&
                Objects.equals(date, friendDTO.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstNameFriend, lastNameFriend, date);
    }

    @Override
    public String toString() {
        return "FriendDTO{" +
                "firstNameFriend='" + firstNameFriend + '\'' +
                ", lastNameFriend='" + lastNameFriend + '\'' +
                ", date=" + date +
                '}';
    }

    /**
     * Prints a header in order to display multiple DTOs as a table
     */
    public static void printHeader() {
        System.out.println(String.format("%20s %5s %20s %5s %20s", "Last name friend", "|", "First name friend",
                "|", "Date of friendship start"));
        System.out.println(String.format("%s", "------------------------------------------------------------------" +
                "------------"));
    }

    /**
     * Prints one entity to fit the header
     */
    public void printFormatted() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String dateFormatted = this.date.format(formatter);
        System.out.println(String.format("%20s %5s %20s %5s %20s", this.getLastNameFriend(), "|",
                this.getFirstNameFriend(), "|", dateFormatted));
    }
}
