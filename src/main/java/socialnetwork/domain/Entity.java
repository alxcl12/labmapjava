package socialnetwork.domain;

import java.io.Serializable;

/**
 * Class used to represent an entity with an ID
 * @param <ID> defining ID of entity(Integer, Long, String, etc.)
 */
public class Entity<ID> implements Serializable {
    private static final long serialVersionUID = 7331115341259248461L;
    private ID id;
    public ID getId() {
        return id;
    }
    public void setId(ID id) {
        this.id = id;
    }
}