package socialnetwork.domain.validators;

/**
 * Interface for a validator used to validate an entity
 * @param <T> type of entity
 */
public interface Validator<T> {
    void validate(T entity) throws ValidationException;
}