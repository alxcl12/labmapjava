/*
 *  @author albua
 *  created on 25/10/2020
 */
package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;

public class FriendshipValidator implements Validator<Friendship> {
    @Override
    public void validate(Friendship entity) throws ValidationException {
        if(entity.getId().getLeft().equals(entity.getId().getRight())){
            throw new ValidationException();
        }
    }
}
