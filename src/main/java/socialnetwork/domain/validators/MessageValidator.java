/*
 *  @author albua
 *  created on 09/11/2020
 */
package socialnetwork.domain.validators;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Message;

/**
 * Validator for Message entity
 */
public class MessageValidator implements Validator<Message> {

    @Override
    public void validate(Message entity) throws ValidationException {

    }
}
