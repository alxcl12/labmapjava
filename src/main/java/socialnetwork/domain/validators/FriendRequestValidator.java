/*
 *  @author albua
 *  created on 08/11/2020
 */
package socialnetwork.domain.validators;

import socialnetwork.domain.FriendRequest;

/**
 * Validator for FriendRequest entity
 */
public class FriendRequestValidator implements Validator<FriendRequest> {

    @Override
    public void validate(FriendRequest entity) throws ValidationException {
        if(entity.getReceiverID().equals(entity.getRequesterID())){
            throw new ValidationException();
        }

        if(!entity.getStatus().equals("Pending") && !entity.getStatus().equals("Approved") && !entity.getStatus().equals("Rejected")){
            throw new ValidationException();
        }
    }
}
