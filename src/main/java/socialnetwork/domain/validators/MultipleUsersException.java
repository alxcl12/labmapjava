/*
 *  @author albua
 *  created on 31/10/2020
 */
package socialnetwork.domain.validators;

/**
 * Class used to signal that there are multiple users with the same first name and last name
 */
public class MultipleUsersException extends RuntimeException {
    public MultipleUsersException() {
    }

    public MultipleUsersException(String message) {
        super(message);
    }

    public MultipleUsersException(String message, Throwable cause) {
        super(message, cause);
    }

    public MultipleUsersException(Throwable cause) {
        super(cause);
    }

    public MultipleUsersException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
