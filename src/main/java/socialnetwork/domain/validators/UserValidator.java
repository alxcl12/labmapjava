package socialnetwork.domain.validators;

import socialnetwork.domain.User;

/**
 * Validates a user via validate method
 */
public class UserValidator implements Validator<User> {

    /**
     * Validates user, last name and name must contain only letters and spaces
     * @param entity user to be validated
     * @throws ValidationException if user is invalid
     */
    @Override
    public void validate(User entity) throws ValidationException {
        if(!entity.getFirstName().matches("[a-zA-Z ]+") || !entity.getLastName().matches("[a-zA-Z ]+")){
            throw new ValidationException("Invalid user!\n");
        }

        if(entity.getFirstName().trim().length() == 0 || entity.getLastName().trim().length() == 0){
            throw new ValidationException("Invalid user!\n");
        }

        if(!entity.getEmail().matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")){
            throw new ValidationException("Invalid email!\n");
        }
    }
}
