/*
 *  @author albua
 *  created on 14/12/2020
 */
package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Event extends Entity<Long> {
    private User owner;
    private List<User> participants;
    private LocalDateTime date;
    private String name;
    private String description;
    private String dateFormat;

    public Event(User owner, LocalDateTime date, String name, String description) {
        this.owner = owner;
        this.date = date;
        this.name = name;
        this.description = description;

        dateFormat = date.format(Constants.DATE_TIME_FORMATTER);
    }

    public static Long generateID(){
        Random randomizer = new Random();
        return randomizer.longs(1000,9999).findFirst().getAsLong();
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
        dateFormat = date.format(Constants.DATE_TIME_FORMATTER);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateFormat(){
        return this.dateFormat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(owner, event.owner) && Objects.equals(participants, event.participants) && Objects.equals(date, event.date) && Objects.equals(name, event.name) && Objects.equals(description, event.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, participants, date, name, description);
    }

    @Override
    public String toString() {
        return "Event{" +
                "owner=" + owner +
                ", participants=" + participants +
                ", date=" + date +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
