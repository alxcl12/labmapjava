/*
 *  @author albua
 *  created on 08/11/2020
 */
package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Class used to model a friend request
 * Status can be either Pending Approved Rejected
 */
public class FriendRequest extends Entity<Tuple<Long, Long>>{
    private String status;
    private Long requesterID;
    private Long receiverID;
    private LocalDateTime date;

    public FriendRequest(String status, Long reqID, Long recID){
        this.status = status;
        this.receiverID = recID;
        this.requesterID = reqID;
        date = LocalDateTime.now();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getRequesterID() {
        return requesterID;
    }

    public void setRequesterID(Long requesterID) {
        this.requesterID = requesterID;
    }

    public Long getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(Long receiverID) {
        this.receiverID = receiverID;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendRequest that = (FriendRequest) o;
        return Objects.equals(status, that.status) &&
                Objects.equals(requesterID, that.requesterID) &&
                Objects.equals(receiverID, that.receiverID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, requesterID, receiverID);
    }
}
