/*
 *  @author albua
 *  created on 08/11/2020
 */
package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Random;
import java.util.List;

/**
 * Class used to model a message from one user to another
 */
public class Message extends Entity<Long>{
    private User from; //sender of message
    private List<User> to; //receiver of message
    private String message; //message to be sent/received
    private LocalDateTime date; //date of the message
    private String dateFormat;
    private String toFormat;

    /**
     * Static method, generates an ID for a message
     * ID is Long with 4 digits
     * @return Generated ID(Long)
     */
    public static Long generateID(){
        Random randomizer = new Random();
        return randomizer.longs(1000,9999).findFirst().getAsLong();
    }

    public Message(User from, List<User> to, String message) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = LocalDateTime.now();

        dateFormat = date.format(Constants.DATE_TIME_FORMATTER);
        if(to.size() > 1){
            this.toFormat = "Group";
        }
        else {
            this.toFormat = to.get(0).toString();
        }
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public List<User> getTo() {
        return to;
    }

    public void setTo(List<User> to) {
        this.to = to;
        if(to.size() > 1){
            toFormat = "Group";
        }
        else {
            toFormat = to.get(0).toString();
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
        dateFormat = date.format(Constants.DATE_TIME_FORMATTER);
    }

    public String getDateFormat(){
        return this.dateFormat;
    }

    public String getToFormat() {
        return toFormat;
    }

    public void setToFormat(String toFormat) {
        this.toFormat = toFormat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(from, message1.from) &&
                Objects.equals(to, message1.to) &&
                Objects.equals(message, message1.message) &&
                Objects.equals(date, message1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, message, date);
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                ", message='" + message + '\'' +
                ", date=" + date +
                '}';
    }
}
