/*
 *  @author albua
 *  created on 08/11/2020
 */
package socialnetwork.domain;


import java.util.List;

/**
 * Class used to model a ReplyMessage
 * Same as Message but it has additional field, message to specify which message is replied
 */
public class ReplyMessage extends Message{
    private Message message;

    public ReplyMessage(User from, List<User> to, String mess, Message message){
        super(from, to, mess);
        this.message = message;
    }

    public Message getMessageReply() {
        return message;
    }

    public void setMessageReply(Message message) {
        this.message = message;
    }
}
