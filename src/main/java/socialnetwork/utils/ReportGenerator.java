/*
 *  @author albua
 *  created on 06/01/2021
 */
package socialnetwork.utils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.service.Service;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class ReportGenerator {
    private PDDocument document;
    private String fileName;
    private String filePath;
    private Service service;
    private PDPage page;
    private int size;
    private int sizeMax;

    public ReportGenerator(String name, String path, Service service){
        this.fileName = name;
        this.filePath = path;
        this.service = service;
        this.document = new PDDocument();
        this.size = 0;
        this.sizeMax = 45;
    }

    public void generateReport1(List<Message> messages, List<Friendship> friendship, Long currentId) throws IOException {
        page = new PDPage();
        document.addPage(page);
        this.size = 0;

        PDPageContentStream pageContentStream = new PDPageContentStream(document, page);
        pageContentStream.setFont(PDType1Font.HELVETICA_BOLD, 17);

        pageContentStream.beginText();
        pageContentStream.newLineAtOffset(50,750);
        pageContentStream.setLeading(16f);

        pageContentStream.showText("Report displaying messages and friendships for current ");
        pageContentStream.newLine();
        pageContentStream.showText("user created in given period.");
        pageContentStream.newLine();
        pageContentStream.newLine();
        pageContentStream.setFont(PDType1Font.HELVETICA, 16);
        pageContentStream.showText("Date of generation: " + LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER));
        pageContentStream.newLine();
        pageContentStream.newLine();

        pageContentStream.setFont(PDType1Font.HELVETICA_BOLD, 15);
        pageContentStream.showText("Messages:");
        pageContentStream.newLine();

        size+=6;
        pageContentStream.setFont(PDType1Font.HELVETICA, 13);
        for(Message x : messages){
            try {
                pageContentStream.showText("From: " + x.getFrom() + "   Message: " + x.getMessage() + "   Date: "
                        + x.getDate().format(Constants.DATE_TIME_FORMATTER));
                pageContentStream.newLine();
                size += 1;
                if(size >= sizeMax){
                    pageContentStream.endText();
                    pageContentStream.close();

                    this.page = new PDPage();
                    document.addPage(page);
                    pageContentStream = new PDPageContentStream(document, page);
                    pageContentStream.setFont(PDType1Font.HELVETICA_BOLD, 17);

                    pageContentStream.beginText();
                    pageContentStream.newLineAtOffset(50,750);
                    pageContentStream.setLeading(16f);
                    size = 0;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        pageContentStream.newLine();

        pageContentStream.setFont(PDType1Font.HELVETICA_BOLD, 15);
        pageContentStream.showText("Friendships:");
        pageContentStream.newLine();
        pageContentStream.setFont(PDType1Font.HELVETICA, 13);

        size+=3;

        for(Friendship x:friendship){
            try {
                if (x.getId().getRight().equals(currentId)) {
                    pageContentStream.showText("New friend: " + service.getUser(x.getId().getLeft()) +
                            "   Since: " + x.getDate().format(Constants.DATE_TIME_FORMATTER));
                } else {
                    pageContentStream.showText("New friend: " + service.getUser(x.getId().getRight()) +
                            "   Since: " + x.getDate().format(Constants.DATE_TIME_FORMATTER));
                }
                pageContentStream.newLine();

                size += 1;
                if(size >= sizeMax){
                    pageContentStream.endText();
                    pageContentStream.close();

                    this.page = new PDPage();
                    document.addPage(page);
                    pageContentStream = new PDPageContentStream(document, page);
                    pageContentStream.setFont(PDType1Font.HELVETICA, 13);

                    pageContentStream.beginText();
                    pageContentStream.newLineAtOffset(50,750);
                    pageContentStream.setLeading(16f);
                    size = 0;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        pageContentStream.endText();
        pageContentStream.close();

        page = new PDPage();
        document.addPage(page);
        PDPageContentStream pageContentStream1 = new PDPageContentStream(document, page);

        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Messages", messages.size());
        dataset.setValue("Friendships", friendship.size());
        JFreeChart chart = ChartFactory.createPieChart("Activities distribution", dataset);
        PiePlot pp = (PiePlot) chart.getPlot();

        //pp.setSectionPaint("Messages", Color.ALICEBLUE);
        //pp.setSectionPaint("Friendships", Color.AQUA);

        BufferedImage bi = chart.createBufferedImage(500, 500);

        PDImageXObject ximage = LosslessFactory.createFromImage(document, bi);

        pageContentStream1.drawImage(ximage, 60, 100);

        pageContentStream1.close();

        document.save(this.filePath + "\\" + this.fileName + ".pdf");
        document.close();
    }

    public void generateReport2(List<Message> messages, User correspondent) throws IOException {
        page = new PDPage();
        document.addPage(page);
        this.size = 0;

        PDPageContentStream pageContentStream = new PDPageContentStream(document, page);
        pageContentStream.setFont(PDType1Font.HELVETICA_BOLD, 17);

        pageContentStream.beginText();
        pageContentStream.newLineAtOffset(50,750);
        pageContentStream.setLeading(16f);

        pageContentStream.showText("Report displaying messages from selected user");
        pageContentStream.newLine();
        pageContentStream.showText("to current user sent in given period.");
        pageContentStream.newLine();
        pageContentStream.newLine();

        pageContentStream.setFont(PDType1Font.HELVETICA, 16);
        pageContentStream.showText("Date of generation: " + LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER));
        pageContentStream.newLine();
        pageContentStream.newLine();

        pageContentStream.setFont(PDType1Font.HELVETICA_BOLD, 15);
        pageContentStream.showText("Correspondent: " + correspondent);
        pageContentStream.newLine();
        pageContentStream.newLine();

        pageContentStream.showText("Messages:");
        pageContentStream.newLine();

        pageContentStream.setFont(PDType1Font.HELVETICA, 13);

        size+=8;
        for(Message x:messages){
            try {
                pageContentStream.showText("Message: " + x.getMessage() + "   Date: "
                        + x.getDate().format(Constants.DATE_TIME_FORMATTER));
                pageContentStream.newLine();

                size += 1;
                if(size >= sizeMax){
                    pageContentStream.endText();
                    pageContentStream.close();

                    this.page = new PDPage();
                    document.addPage(page);
                    pageContentStream = new PDPageContentStream(document, page);
                    pageContentStream.setFont(PDType1Font.HELVETICA, 13);

                    pageContentStream.beginText();
                    pageContentStream.newLineAtOffset(50,750);
                    pageContentStream.setLeading(16f);
                    size = 0;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        pageContentStream.newLine();

        pageContentStream.endText();
        pageContentStream.close();

        document.save(this.filePath + "\\" + this.fileName + ".pdf");
        document.close();
    }
}
