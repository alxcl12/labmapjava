/*
 *  @author albua
 *  created on 24/10/2020
 */
package socialnetwork.utils;

import java.util.*;

/**
 * Class used to represent directed graph
 * @param <E> type of elements to be stored inside the graph vertexes
 */
public class Graph<E> {
    private Map<Vertex<E>, List<Vertex<E>>> adj;

    public Graph(){
        adj = new HashMap<>();
    }

    /**
     * Method adds new vertex to the graph
     * @param value to be contained in the vertex
     */
    public void addVertex(E value){
        try {
            adj.putIfAbsent(new Vertex<>(value), new ArrayList<>());
        }catch (NullPointerException e){
            e.getMessage();
        }
    }

    /**
     * Method removes vertex from the graph
     * @param value that is contained inside the vertex to be removed
     */
    public void removeVertex(E value){
        Vertex<E> test = new Vertex<>(value);

        //remove vertex value from each adjacency list
        adj.values().forEach(elem -> elem.remove(test));

        //remove vertex
        adj.remove(test);
    }

    /**
     * Adds new edge to the graph
     * @param value1 value of first vertex
     * @param value2 value of second vertex
     */
    public void addEdge(E value1, E value2){
        Vertex<E> v1 = new Vertex<>(value1);
        Vertex<E> v2 = new Vertex<>(value2);

        //add to both of the vertexes lists, graph is directed
        adj.get(v1).add(v2);
        adj.get(v2).add(v1);
    }

    /**
     * Remove edge from graph
     * @param value1 value of first vertex
     * @param value2 value of second vertex
     */
    public void removeEdge(E value1, E value2){
        Vertex<E> v1 = new Vertex<>(value1);
        Vertex<E> v2 = new Vertex<>(value2);

        List<Vertex<E>> list1 = adj.get(v1);
        List<Vertex<E>> list2 = adj.get(v2);

        //since graph is directed, remove from both lists
        if(list1 != null){
            list1.remove(v2);
        }

        if(list2 != null){
            list2.remove(v1);
        }


        //if node became singular, remove it
        if(list1 == null || list1.size() == 0){
            removeVertex(value1);
        }

        if(list2 == null || list2.size() == 0){
            removeVertex(value2);
        }
    }

    /**
     * Method performs DFS of the graph and returns a map containing all vertexes as key
     * and an integer as value, corresponding to its connected component
     * @return Map<Vertex, Integer>
     */
    private Map<Vertex<E>, Integer> DFS(){
        Set<Vertex<E>> visited = new HashSet<>();
        Stack<Vertex<E>> stack = new Stack<>();

        //create a map to memorise each vertex and its component
        Map<Vertex<E>, Integer> components = new HashMap<>();
        for(Vertex<E> v: adj.keySet()){
            //0 means it is not assigned
            components.put(v, 0);
        }

        int count = 0; //counter for components

        //do DFS for all vertexes
        while(visited.size() != adj.size()) {
            //new "code" for the connected component
            count++;

            //find an unassigned vertex
            for(Vertex<E> v: components.keySet()){
                //0 means it is not assigned
                if(components.get(v) == 0){
                    components.put(v, count);
                    stack.push(v);
                    break;
                }
            }

            while (!stack.isEmpty()) {
                Vertex<E> value = stack.pop();

                if (!visited.contains(value)) {
                    visited.add(value);
                    List<Vertex<E>> list = adj.get(value);

                    for (Vertex<E> v : list) {
                        stack.push(v);
                        components.put(v, components.get(value));
                    }
                }
            }
        }

        return components;
    }

    /**
     * Method returns number of connected components of the graph
     * @return integer
     */
    public int getNrConnectedComponents(){
        int size = adj.size();

        //if graph is empty
        if(size == 0){
            return 0;
        }

        Map<Vertex<E>, Integer> result = DFS();

        //finding how many single vertexes are in there
        long toSubtract = adj.entrySet().stream().filter(x -> x.getValue().size() == 0).count();

        //since components are labeled from 1, max value will be the number of connected components - number of single vertexes
        return Collections.max(result.values()) - (int)toSubtract;
    }

    /**
     * Returns an ArrayList containing the values of the vertexes belonging to the maximum connected component
     * @return ArrayList of vertex values
     */
    public ArrayList<E> getMaxComponent(){

        Map<Vertex<E>, Integer> result = DFS();
        //transforming map values to set so finding frequency of each value is more convenient
        Set<Integer> set = new HashSet<>(result.values());

        //initial values for maximum frequency and its value
        int maxCount = 0;
        int maxCountVal = 0;

        for(Integer val: set){
            int count = Collections.frequency(result.values(), val);

            if(count > maxCount){
                maxCount = count;
                maxCountVal = val;
            }
        }

        ArrayList<E> toReturn = new ArrayList<>();

        //adding all corresponding values to arraylist
        for(Vertex<E> v: result.keySet()){
            if(result.get(v) == maxCountVal){
                toReturn.add(v.getValue());
            }
        }

        return toReturn;
    }

    /**
     * Checks if there is a possible elementary path represented inside the stack
     * @param stack stack from the backtracking, contains indexes for list
     * @param list list containing all vertexes, used with stack to construct the path
     * @return true if path is valid false otherwise
     */
    private boolean isPath(Stack<Integer> stack, ArrayList<Vertex<E>> list){
        //only one vertex can form a path
        if(stack.size() == 1){
            return true;
        }

        int aux = 0;
        aux = stack.pop();

        Vertex<E> vertex = list.get(aux);

        //if there is no edge from previous to current vertex
        if(!adj.get(vertex).contains(list.get(stack.peek()))){
            //restore the stack
            stack.push(aux);
            return false;
        }

        //check if path is elementary(all vertexes are unique)
        int result = stack.search(aux);

        //restore stack
        stack.push(aux);

        //all vertexes are unique
        return result == -1;
    }

    /**
     * Copies the corresponding vertexes from stack and keyList into returnList
     * @param stack stack from the backtracking, contains indexes for list
     * @param keyList list containing all vertexes, used with stack to construct the path
     * @param returnList list containing vertexes that are represented on the current path
     */
    private void copyStack(Stack<Integer> stack, ArrayList<Vertex<E>> keyList, ArrayList<Vertex<E>> returnList){
        //auxiliary stack used for copying
        Stack<Integer> auxStack = new Stack<>();

        //empty original stack and fill auxiliary
        while (!stack.empty()){
            auxStack.push(stack.pop());
        }

        //fill returnList and the original stack back
        while(!auxStack.empty()){
            int top = auxStack.pop();
            stack.push(top);
            returnList.add(keyList.get(top));
        }
    }

    /**
     * Returns an elementary path starting from vertex with label startPoint and ending with vertex labeled with
     * endPoint(maximum length)
     * @param startPoint start of path
     * @param endPoint   end of path
     * @return ArrayList of vertexes, forming the elementary path
     */
    public ArrayList<Vertex<E>> getElementaryPath(E startPoint, E endPoint){
        //stack used for backtracking
        Stack<Integer> stack = new Stack<>();

        //set containing all keys, will be used with an array list and the backtracking stack
        Set<Vertex<E>> keySet = adj.keySet();

        //initialise maxSize of path
        int maxSize = -1;
        //list with vertexes that are in the path
        ArrayList<Vertex<E>> toReturn = new ArrayList<>();

        //list containing all vertexes, the stack will contain indexes from this list
        ArrayList<Vertex<E>> keyList = new ArrayList<>(keySet);


        //now just make sure that startPoint is at the beginning of the list
        int toSwap = 0;
        for(int i=0; i<keyList.size(); i++){
            if(keyList.get(i).getValue() == startPoint){
                toSwap = i;
                break;
            }
        }

        Collections.swap(keyList, toSwap, 0);

        //finding path from first element(startPoint)
        stack.push(0);
        stack.push(0);
        while(!stack.empty()){
            int aux = stack.pop();
            aux++;
            stack.push(aux);

            if(stack.peek() >= keyList.size()){
                //backtrack
                stack.pop();
            }
            else {
                //validation function
                if(this.isPath(stack, keyList)){
                    //path is from startPoint to endPoint
                    if(keyList.get(stack.peek()).getValue() == endPoint){
                        //path is of maximum size
                        if(stack.size() > maxSize){
                            maxSize = stack.size();
                            toReturn.clear();
                            this.copyStack(stack, keyList, toReturn);
                        }
                    }
                    else{
                        //keep searching
                        stack.push(0);
                    }
                }
            }
        }

        return toReturn;
    }

    /**
     * Returns longest elementary path from the graph
     * @return ArrayList that contains vertexes that belong to the longest elementary path
     */
    public ArrayList<Vertex<E>> getLongestElementaryPath(){
        //set containing all vertexes
        Set<Vertex<E>> keySet = adj.keySet();

        //making the set an array list so no redundant calculations are made
        ArrayList<Vertex<E>> keyList = new ArrayList<>(keySet);

        //list with vertexes that are in the path
        ArrayList<Vertex<E>> toReturn = new ArrayList<>();

        //auxiliary list used for finding the longest path
        ArrayList<Vertex<E>> aux = new ArrayList<>();

        int maxLen = -1;

        for(int i=0; i<keyList.size(); i++){
            for(int j=i+1; j<keyList.size(); j++){
                aux = this.getElementaryPath(keyList.get(i).getValue(), keyList.get(j).getValue());

                //check if path is maximum
                if(aux.size() > maxLen){
                    maxLen = aux.size();
                    toReturn.clear();
                    toReturn.addAll(aux);
                    aux.clear();
                }
            }
        }

        return toReturn;
    }

}
