package socialnetwork.utils;

public interface Observer {
    void update();
}
