/*
 *  @author albua
 *  created on 24/10/2020
 */
package socialnetwork.utils;

import java.util.Objects;

/**
 * Class used to represent a vertex in a directed graph
 * @param <E> type of information to be stored in a vertex
 */
public class Vertex<E> {
    private E value;
    public Vertex(E val){
        this.value = val;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex<?> vertex = (Vertex<?>) o;
        return Objects.equals(value, vertex.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
