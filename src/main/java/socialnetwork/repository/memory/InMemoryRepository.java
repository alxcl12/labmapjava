package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Class used to store entities inside memory
 * Must contain a validator for the entity
 * @param <ID> id of each entity
 * @param <E>  entity
 */
public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E>{
    private Validator<E> validator;
    protected Map<ID,E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities= new HashMap<>();
    }

    /**
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return entity of corresponding ID
     */
    @Override
    public Optional<E> findOne(ID id){
        if (id == null) {
            throw new IllegalArgumentException("ID must be not null");
        }
        return Optional.ofNullable(entities.get(id));
    }

    /**
     * @return Iterable containing all entities
     */
    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     * Stores an entity into repository
     * @param entity
     *         entity must be not null
     * @return null if entity is stored, input entity if it already exists
     */
    @Override
    public Optional<E> save(E entity) {
        if (entity==null){
            throw new IllegalArgumentException("Entity must be not null");
        }

        validator.validate(entity);

        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    /**
     * Removes entity of given ID from repository
     * @param id
     *      id must be not null
     * @return deleted entity or null if it did not exist
     */
    @Override
    public Optional<E> delete(ID id) {
        if(id == null){
            throw new IllegalArgumentException("ID cannot be null!\n");
        }

        return Optional.ofNullable(entities.remove(id));
    }

    /**
     * Updates existing entity
     * @param entity
     *          entity must not be null
     * @return null if entity was updated successfully or entity if it did not exist in the first place
     */
    @Override
    public Optional<E> update(E entity) {
        if(entity == null) {
            throw new IllegalArgumentException("Entity must be not null!");
        }

        validator.validate(entity);

        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k,v) -> entity));
    }

}
