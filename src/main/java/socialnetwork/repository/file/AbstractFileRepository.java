package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


/**
 * Abstract class used for file repositories
 * Each repository that inherits this class should implement extractEntity and createEntityAsString(template pattern)
 * @param <ID> ID of entities stored in files
 * @param <E> Entity to be stored in file
 */
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {
    String fileName; //path to file

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName=fileName;
        loadData();
    }

    /**
     * Loads data into memory when repository is created
     */
    private void loadData() {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while((line = br.readLine()) != null){
                List<String> attr = Arrays.asList(line.split(";"));
                E e = extractEntity(attr);
                super.save(e);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *  extract entity  - template method design pattern
     *  creates an entity of type E having a specified list of @code attributes
     * @param attributes fields of entity to be extracted
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);

    /**
     * Creates a string from an entity to be stored inside the file
     * @param entity entity to be stored
     * @return string consisting of the entity
     */
    protected abstract String createEntityAsString(E entity);

    @Override
    public Optional<E> save(E entity){
        Optional<E> e = super.save(entity);

        if (!e.isPresent()) {
            writeToFile();
        }

        return e;
    }

    @Override
    public Optional<E> delete(ID id){
        Optional<E> e = super.delete(id);

        if(e.isPresent()){
            writeToFile();
        }

        return e;
    }

    @Override
    public Optional<E> update(E entity){
        Optional<E> e = super.update(entity);

        if(!e.isPresent()){
            writeToFile();
        }

        return e;
    }

    /**
     * Stores entities into file
     */
    protected void writeToFile(){
        try (BufferedWriter bW = new BufferedWriter(new FileWriter(fileName))) {
            for(E e: this.entities.values()){
                bW.write(createEntityAsString(e));
                bW.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

