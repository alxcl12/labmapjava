/*
 *  @author albua
 *  created on 01/11/2020
 */
package socialnetwork.repository.file;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository to store users into file
 */
public class FriendshipFile extends AbstractFileRepository<Tuple<Long,Long>, Friendship>{

    public FriendshipFile(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    /**
     * Method constructs a friendship from the input string (id1, id2)
     * @param attributes fields of entity to be extracted
     * @return created Friendship
     */
    @Override
    public Friendship extractEntity(List<String> attributes) {
        Friendship entity = new Friendship();
        Tuple<Long, Long> tupleId = new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)));

        entity.setId(tupleId);
        entity.setDate(LocalDateTime.parse(attributes.get(2)));

        return entity;
    }

    /**
     * Builds friendship as string to be stored in file
     * @param entity entity to be stored
     * @return created string
     */
    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getId().getLeft() + ";" + entity.getId().getRight() + ";" + entity.getDate();
    }
}
