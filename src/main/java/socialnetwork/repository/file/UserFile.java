package socialnetwork.repository.file;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;

import java.util.List;

/**
 * Repository to store users into file
 */
public class UserFile extends AbstractFileRepository<Long, User>{

    public UserFile(String fileName, Validator<User> validator) {
        super(fileName, validator);
    }

    /**
     * Method constructs a user from the input string (id, firstName, lastName)
     * @param attributes fields of entity to be extracted
     * @return created User
     */
    @Override
    public User extractEntity(List<String> attributes) {
        User entity  = new User(attributes.get(1), attributes.get(2), attributes.get(3), attributes.get(4));
        entity.setId(Long.parseLong(attributes.get(0)));
        return entity;
    }

    /**
     * Builds user as string to be stored in file
     * @param entity entity to be stored
     * @return created string
     */
    @Override
    protected String createEntityAsString(User entity) {
        return entity.getId() + ";" + entity.getFirstName() + ";" + entity.getLastName() + ";" + entity.getEmail() + ";" + entity.getPassword();
    }
}
