package socialnetwork.repository.paging;

import socialnetwork.domain.Entity;
import socialnetwork.repository.Repository;

public interface PagingRepository<ID, E extends Entity<ID>> extends Repository<ID, E> {
    Page<E> findAll(Pageable pageable);

    Iterable<E> findOnPage(int page);

    void setPageSize(int size);

    Iterable<E> findOnPageNon(int page, Long currentID);

    Iterable<E> findOnPageFriends(int page, Long currentID);
}