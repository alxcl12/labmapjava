/*
 *  @author albua
 *  created on 13/12/2020
 */
package socialnetwork.repository.paging;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Paginator<E> {
    private Pageable pageable;
    private Iterable<E> content;

    public Paginator(Pageable pageable, Iterable<E> elements){
        this.pageable = pageable;
        this.content = elements;
    }

    public Page<E> paginate(){
        Stream<E> result = StreamSupport.stream(content.spliterator(), false)
                .skip((long) pageable.getPageNumber() * pageable.getPageSize())
                .limit(pageable.getPageSize());

        return new PageImplementation<>(pageable, result);
    }
}
