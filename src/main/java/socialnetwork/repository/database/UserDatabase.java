/*
 *  @author albua
 *  created on 01/11/2020
 */
package socialnetwork.repository.database;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.sql.DriverManager;
import java.util.HashSet;
import java.util.Optional;

/**
 * Database repository for users
 * Designed for use with POSTGRESQL database
 */
public class UserDatabase implements PagingRepository<Long, User> {
    //url to database
    private final String url;

    //username for database
    private final String username;

    //password to database
    private final String password;
    private final Validator<User> validator;

    private int pageSize;

    Connection conn;

    public UserDatabase(String url, String username, String password, Validator<User> validator){
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;

        try {
            this.conn = DriverManager.getConnection(url, username, this.password);
        }
        catch (SQLException e){
            e.printStackTrace();
        }

    }

    public Optional<User> getByEmailPass(String email, String password){
        try {
            if(email.equals("") || password.equals("")){
                return Optional.empty();
            }
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"Users\"" +
                    "WHERE \"Email\"=" + "'" + email + "' AND \"Password\"=crypt('" + password + "'," +
                    "\"Password\");");


            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String Email = resultSet.getString("Email");
                String Password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, Email, Password);
                usr.setId(id);

                return Optional.of(usr);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Optional<User> findOne(Long aLong) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"Users\"" +
                    "WHERE \"ID\"=" + "'" + aLong + "'");

            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                String password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, email, password);
                usr.setId(id);

                return Optional.of(usr);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Iterable<User> findAll() {
        HashSet<User> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"Users\"");

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                String password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, email, password);
                usr.setId(id);

                toReturn.add(usr);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public Optional<User> save(User entity) {
        validator.validate(entity);

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement statementEmail = conn.prepareStatement("SELECT * FROM \"Users\"" +
                    "WHERE \"Email\"='" + entity.getEmail() + "';");
            ResultSet resultSet = statementEmail.executeQuery();
            if(resultSet.next()){
                throw new ValidationException("Email taken!\n");
            }


            PreparedStatement statement = conn.prepareStatement("INSERT INTO " +
                    "\"Users\"(\"ID\", \"FirstName\", \"LastName\", \"Email\", \"Password\") " +
                    "VALUES('" + entity.getId().toString() + "','" + entity.getFirstName() + "','" +
                    entity.getLastName() + "','" + entity.getEmail() + "',crypt('" + entity.getPassword() +
                    "', gen_salt('bf',8)));");

            statement.execute();

        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.of(entity);
        }

        return Optional.empty();
    }

    @Override
    public Optional<User> delete(Long aLong) {
        User usr = new User("ad", "sad", "sadsfas", "dasfdsfsdf");

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            //check if user exists to be deleted, so deleted user can be returned
            Optional<User> opt = this.findOne(aLong);

            if(opt.isPresent()){
                usr = opt.get();
            }
            else {
                return Optional.empty();
            }

            PreparedStatement statement2 = conn.prepareStatement("DELETE FROM " +
                    "\"Users\"" +
                    "WHERE \"ID\" =" + "'" + aLong + "'");

            statement2.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.of(usr);
    }

    @Override
    public Optional<User> update(User entity) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement statement = conn.prepareStatement("UPDATE " +
                    "\"Users\"" +
                    "SET \"FirstName\"='" + entity.getFirstName() + "', \"LastName\"='" + entity.getLastName() + "'" +
                    "WHERE \"ID\" =" + "'" + entity.getId() + "'");

            statement.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.ofNullable(entity);
        }

        return Optional.empty();
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        Paginator<User> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }

    @Override
    public void setPageSize(int size){
        this.pageSize = size;
    }

    @Override
    public Iterable<User> findOnPage(int page) {
        HashSet<User> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"Users\"" +
                    "ORDER BY \"ID\" OFFSET " + pageSize*page + " LIMIT " + pageSize);

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                String password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, email, password);
                usr.setId(id);

                toReturn.add(usr);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public Iterable<User> findOnPageNon(int page, Long currentID){
        HashSet<User> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement findAllStatement = conn.prepareStatement("SELECT u2.\"ID\", u2.\"FirstName\"," +
                            "u2.\"LastName\",u2.\"Email\",u2.\"Password\"" +
                    "FROM \"Users\" u1,\"Users\" u2" +
                    " WHERE NOT EXISTS(SELECT 1 FROM \"FriendRequests\" f"
                    +        " WHERE ("
                    +                "(f.\"RequesterID\" = u1.\"ID\" AND f.\"ReceiverID\" = u2.\"ID\") or"
                    +                "(f.\"RequesterID\" = u2.\"ID\" AND f.\"ReceiverID\" = u1.\"ID\")))"
            + "AND u1.\"ID\" != u2.\"ID\" AND u1.\"ID\" ='" + currentID + "' ORDER BY \"ID\" OFFSET " + pageSize*page
            + " LIMIT " + pageSize);

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                String password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, email, password);
                usr.setId(id);

                toReturn.add(usr);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public Iterable<User> findOnPageFriends(int page, Long currentID){
        HashSet<User> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement findAllStatement = conn.prepareStatement("SELECT u2.\"ID\", u2.\"FirstName\"," +
                    "u2.\"LastName\",u2.\"Email\",u2.\"Password\"" +
                    "FROM \"Users\" u2" +
                    " WHERE EXISTS(SELECT 1 FROM \"Friendships\" f"
                    +        " WHERE ("
                    +                "(f.\"SenderID\" ='" + currentID +"' AND f.\"ReceiverID\" = u2.\"ID\") or"
                    +                "(f.\"SenderID\" = u2.\"ID\" AND f.\"ReceiverID\" ='" + currentID + "')))"
                    + "AND '" + currentID + "' != u2.\"ID\" ORDER BY \"ID\" OFFSET " + pageSize*page
                    + " LIMIT " + pageSize);

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                String password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, email, password);
                usr.setId(id);

                toReturn.add(usr);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }
}
