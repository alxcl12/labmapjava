/*
 *  @author albua
 *  created on 08/11/2020
 */
package socialnetwork.repository.database;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;

/**
 * Database repository for friend requests
 * Designed for use with POSTGRESQL database
 */
public class FriendRequestDatabase implements PagingRepository<Tuple<Long, Long>, FriendRequest> {
    //url to database
    private final String url;

    //username for database
    private final String username;

    //password to database
    private final String password;
    private final Validator<FriendRequest> validator;

    private int pageSize;

    Connection conn;

    public FriendRequestDatabase(String url, String username, String password, Validator<FriendRequest> validator){
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.conn = DriverManager.getConnection(url, username, password);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<FriendRequest> findOne(Tuple<Long, Long> stringStringTuple) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"FriendRequests\"" +
                    "WHERE \"RequesterID\"=" + "'" + stringStringTuple.getLeft() + "'" + "AND \"ReceiverID\"=" + "'" + stringStringTuple.getRight() + "';");

            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id1 = resultSet.getLong("RequesterID");
                Long id2 = resultSet.getLong("ReceiverID");
                String status = resultSet.getString("Status");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();

                Tuple<Long, Long> tuple = new Tuple<>(id1,id2);
                FriendRequest friendRequest = new FriendRequest(status, id1, id2);
                friendRequest.setId(tuple);
                friendRequest.setDate(time);

                return Optional.of(friendRequest);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Iterable<FriendRequest> findAll() {
        HashSet<FriendRequest> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"FriendRequests\"");

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id1 = resultSet.getLong("RequesterID");
                Long id2 = resultSet.getLong("ReceiverID");
                String status = resultSet.getString("Status");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();

                Tuple<Long, Long> tuple = new Tuple<>(id1,id2);
                FriendRequest friendRequest = new FriendRequest(status, id1, id2);
                friendRequest.setId(tuple);
                friendRequest.setDate(time);

                toReturn.add(friendRequest);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public Optional<FriendRequest> save(FriendRequest entity) {
        validator.validate(entity);

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            String[] time = entity.getDate().toString().split("T");

            PreparedStatement statement = conn.prepareStatement("INSERT INTO " +
                    "\"FriendRequests\"(\"RequesterID\", \"ReceiverID\", \"Status\", \"Date\") " +
                    "VALUES('" + entity.getRequesterID() + "','" + entity.getReceiverID() + "','" + entity.getStatus() + "','" + time[0] + " " + time[1] + "');");

            statement.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.of(entity);
        }

        return Optional.empty();
    }

    @Override
    public Optional<FriendRequest> delete(Tuple<Long, Long> stringStringTuple) {
        FriendRequest friendRequest = new FriendRequest("as", 21L,2L);

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            //check if friend request exists to be deleted, so deleted friend request can be returned
            Optional<FriendRequest> opt = this.findOne(stringStringTuple);

            if(opt.isPresent()){
                friendRequest = opt.get();
            }
            else {
                return Optional.empty();
            }

            PreparedStatement statement2 = conn.prepareStatement("DELETE FROM " +
                    "\"FriendRequests\"" +
                    "WHERE \"RequesterID\" =" + "'" + stringStringTuple.getLeft() + "' AND \"ReceiverID\" =" + "'" + stringStringTuple.getRight() + "';");

            statement2.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.of(friendRequest);
    }

    @Override
    public Optional<FriendRequest> update(FriendRequest entity) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement statement = conn.prepareStatement("UPDATE " +
                    "\"FriendRequests\"" +
                    "SET \"Status\"='" + entity.getStatus() + "' " +
                    "WHERE \"RequesterID\" =" + "'" + entity.getId().getLeft() + "' AND \"ReceiverID\"='" + entity.getId().getRight() + "';");

            statement.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.ofNullable(entity);
        }

        return Optional.empty();
    }

    @Override
    public Page<FriendRequest> findAll(Pageable pageable) {
        Paginator<FriendRequest> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }

    @Override
    public Iterable<FriendRequest> findOnPage(int page) {
        HashSet<FriendRequest> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"FriendRequests\"" +
                    " OFFSET " + pageSize*page + " LIMIT " + pageSize);

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id1 = resultSet.getLong("RequesterID");
                Long id2 = resultSet.getLong("ReceiverID");
                String status = resultSet.getString("Status");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();

                Tuple<Long, Long> tuple = new Tuple<>(id1,id2);
                FriendRequest friendRequest = new FriendRequest(status, id1, id2);
                friendRequest.setId(tuple);
                friendRequest.setDate(time);

                toReturn.add(friendRequest);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public void setPageSize(int size) {
        this.pageSize = size;
    }

    @Override
    public Iterable<FriendRequest> findOnPageNon(int page, Long currentID){
        return new HashSet<>();
    }

    public Iterable<FriendRequest> findOnPageFriends(int page, Long currentID){
        return new HashSet<>();
    }
}