/*
 *  @author albua
 *  created on 02/11/2020
 */
package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;

/**
 * Database repository for friendships
 * Designed for use with POSTGRESQL database
 */
public class FriendshipDatabase implements Repository<Tuple<Long, Long>, Friendship> {
    //url to database
    private String url;

    //username for database
    private String username;

    //password to database
    private String password;
    private Validator<Friendship> validator;

    Connection conn;

    public FriendshipDatabase(String url, String username, String password, Validator<Friendship> validator){
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.conn = DriverManager.getConnection(url, username, password);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Friendship> findOne(Tuple<Long, Long> stringStringTuple) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"Friendships\"" +
                    "WHERE \"SenderID\"=" + "'" + stringStringTuple.getLeft() + "'" + "AND \"ReceiverID\"=" + "'" + stringStringTuple.getRight() + "';");

            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id1 = resultSet.getLong("SenderID");
                Long id2 = resultSet.getLong("ReceiverID");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();

                Tuple<Long, Long> tuple = new Tuple<>(id1,id2);
                Friendship friendship = new Friendship();
                friendship.setId(tuple);
                friendship.setDate(time);

                return Optional.of(friendship);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Iterable<Friendship> findAll() {
        HashSet<Friendship> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"Friendships\"");

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id1 = resultSet.getLong("SenderID");
                Long id2 = resultSet.getLong("ReceiverID");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();

                Tuple<Long, Long> tuple = new Tuple<>(id1,id2);
                Friendship friendship = new Friendship();
                friendship.setId(tuple);
                friendship.setDate(time);

                toReturn.add(friendship);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public Optional<Friendship> save(Friendship entity) {
        validator.validate(entity);

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            String[] time = entity.getDate().toString().split("T");

            PreparedStatement statement = conn.prepareStatement("INSERT INTO " +
                    "\"Friendships\"(\"SenderID\", \"ReceiverID\", \"Date\") " +
                    "VALUES('" + entity.getId().getLeft() + "','" + entity.getId().getRight() + "','" + time[0] + " " + time[1] + "');");

            statement.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.of(entity);
        }

        return Optional.empty();
    }

    @Override
    public Optional<Friendship> delete(Tuple<Long, Long> stringStringTuple) {
        Friendship friendship = new Friendship();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            //check if user exists to be deleted, so deleted user can be returned
            Optional<Friendship> opt = this.findOne(stringStringTuple);

            if(opt.isPresent()){
                friendship = opt.get();
            }
            else {
                return Optional.empty();
            }

            PreparedStatement statement2 = conn.prepareStatement("DELETE FROM " +
                    "\"Friendships\"" +
                    "WHERE \"SenderID\" =" + "'" + stringStringTuple.getLeft() + "' AND \"ReceiverID\" =" + "'" + stringStringTuple.getRight() + "';");

            statement2.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.of(friendship);
    }

    @Override
    public Optional<Friendship> update(Friendship entity) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement statement = conn.prepareStatement("UPDATE " +
                    "\"Friendships\"" +
                    "SET \"Date\"='" + entity.getDate() + "' " +
                    "WHERE \"SenderID\" =" + "'" + entity.getId().getLeft() + "' AND \"ReceiverID\"='" + entity.getId().getRight() + "';");

            statement.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.ofNullable(entity);
        }

        return Optional.empty();
    }
}
