/*
 *  @author albua
 *  created on 14/12/2020
 */
package socialnetwork.repository.database;

import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;

/**
 * Database repository for events
 * Designed for use with POSTGRESQL database
 */
public class EventDatabase implements PagingRepository<Long, Event> {
    //url to database
    private final String url;

    //username for database
    private final String username;

    //password to database
    private final String password;
    private final Validator<Event> validator;

    private int pageSize;

    Connection conn;

    public EventDatabase(String url, String username, String password, Validator<Event> validator){
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.conn = DriverManager.getConnection(url, username, password);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    private User findUser(Long aLong){
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"Users\"" +
                    "WHERE \"ID\"=" + "'" + aLong + "'");

            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                String password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, email, password);
                usr.setId(id);

                conn.close();
                return usr;
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return null;
    }

    @Override
    public Optional<Event> findOne(Long aLong){
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"events\"" +
                    "WHERE \"id\"=" + "'" + aLong + "'");

            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id = resultSet.getLong("id");
                Long ownerID = resultSet.getLong("ownerid");
                String name = resultSet.getString("name");
                LocalDateTime time = resultSet.getTimestamp("date").toLocalDateTime();
                String description = resultSet.getString("description");

                User fromUsr = this.findUser(ownerID);
                ArrayList<User> toUsr = new ArrayList<>();

                PreparedStatement createList = conn.prepareStatement("SELECT \"userid\" FROM " +
                        "\"eventsusers\" WHERE \"eventid\" = '" + id + "';");
                ResultSet usrs = createList.executeQuery();
                while (usrs.next()){
                    Long idU = usrs.getLong("userid");
                    toUsr.add(this.findUser(idU));
                }


                Event event;

                event = new Event(fromUsr, time, name, description);

                event.setDate(time);
                event.setId(id);
                event.setParticipants(toUsr);

                return Optional.of(event);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }


        return Optional.empty();
    }

    @Override
    public Iterable<Event> findAll() {
        HashSet<Event> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"events\"");

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("id");
                Long ownerID = resultSet.getLong("ownerid");
                String name = resultSet.getString("name");
                LocalDateTime time = resultSet.getTimestamp("date").toLocalDateTime();
                String description = resultSet.getString("description");


                User fromUsr = this.findUser(ownerID);
                ArrayList<User> toUsr = new ArrayList<>();

                PreparedStatement createList = conn.prepareStatement("SELECT \"userid\" FROM " +
                        "\"eventsusers\" WHERE \"eventid\" = '" + id + "';");

                ResultSet usrs = createList.executeQuery();
                while (usrs.next()){
                    Long idU = usrs.getLong("UserID");
                    toUsr.add(this.findUser(idU));
                }


                Event event;

                event = new Event(fromUsr, time, name, description);

                event.setDate(time);
                event.setId(id);
                event.setParticipants(toUsr);

                toReturn.add(event);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public Optional<Event> save(Event entity) {
        validator.validate(entity);

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            String[] time = entity.getDate().toString().split("T");

            PreparedStatement statement;


                statement = conn.prepareStatement("INSERT INTO " +
                        "\"events\"(\"id\", \"ownerid\", \"name\", \"date\", description) " +
                        "VALUES('" + entity.getId() + "','" + entity.getOwner().getId() + "','"
                        + entity.getName() + "','" + time[0] + " " + time[1]  + "','"
                        + entity.getDescription() +"');");

            statement.execute();

            if(entity.getParticipants() != null) {
                for (User u : entity.getParticipants()) {
                    PreparedStatement relation = conn.prepareStatement("INSERT INTO" +
                            "\"eventsusers\"(\"userid\", \"eventid\") " +
                            "VALUES ('" + u.getId() + "','" + entity.getId() + "');");
                    relation.execute();
                }
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.of(entity);
        }

        return Optional.empty();
    }

    @Override
    public Optional<Event> delete(Long aLong) {
        Event msg = null;

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }
            //check if user exists to be deleted, so deleted user can be returned
            Optional<Event> opt = this.findOne(aLong);

            if(opt.isPresent()){
                msg = opt.get();
            }
            else {
                return Optional.empty();
            }

            PreparedStatement statement2 = conn.prepareStatement("DELETE FROM " +
                    "\"events\"" +
                    "WHERE \"id\" =" + "'" + aLong + "'");

            statement2.execute();

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.ofNullable(msg);
    }

    @Override
    public Optional<Event> update(Event entity) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement statement = conn.prepareStatement("DELETE FROM eventsusers WHERE eventid='" +
                    + entity.getId() + "';");

            statement.execute();

            if(entity.getParticipants() != null) {
                for (User u : entity.getParticipants()) {
                    PreparedStatement relation = conn.prepareStatement("INSERT INTO" +
                            "\"eventsusers\"(\"userid\", \"eventid\") " +
                            "VALUES ('" + u.getId() + "','" + entity.getId() + "');");
                    relation.execute();
                }
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.ofNullable(entity);
        }


        return Optional.empty();
    }

    @Override
    public Page<Event> findAll(Pageable pageable) {
        Paginator<Event> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }

    @Override
    public Iterable<Event> findOnPage(int page) {
        HashSet<Event> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"events\"" +
                    "ORDER BY \"id\" OFFSET " + pageSize*page + " LIMIT " + pageSize);

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("id");
                Long ownerID = resultSet.getLong("ownerid");
                String name = resultSet.getString("name");
                LocalDateTime time = resultSet.getTimestamp("date").toLocalDateTime();
                String description = resultSet.getString("description");


                User fromUsr = this.findUser(ownerID);
                ArrayList<User> toUsr = new ArrayList<>();

                PreparedStatement createList = conn.prepareStatement("SELECT \"userid\" FROM " +
                        "\"eventsusers\" WHERE \"eventid\" = '" + id + "';");

                ResultSet usrs = createList.executeQuery();
                while (usrs.next()){
                    Long idU = usrs.getLong("UserID");
                    toUsr.add(this.findUser(idU));
                }


                Event event;

                event = new Event(fromUsr, time, name, description);

                event.setDate(time);
                event.setId(id);
                event.setParticipants(toUsr);

                toReturn.add(event);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public void setPageSize(int size) {
        this.pageSize = size;
    }

    public Iterable<Event> findOnPageNon(int page, Long currentID){
        return new HashSet<>();
    }

    @Override
    public Iterable<Event> findOnPageFriends(int page, Long currentID){
        return new HashSet<>();
    }
}
