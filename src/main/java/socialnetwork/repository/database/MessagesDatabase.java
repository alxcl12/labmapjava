/*
 *  @author albua
 *  created on 09/11/2020
 */
package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.ReplyMessage;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;

/**
 * Database repository for messages
 * Designed for use with POSTGRESQL database
 */
public class MessagesDatabase implements PagingRepository<Long, Message> {
    //url to database
    private final String url;

    //username for database
    private final String username;

    //password to database
    private final String password;
    private final Validator<Message> validator;

    private int pageSize;

    Connection conn;

    public MessagesDatabase(String url, String username, String password, Validator<Message> validator){
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        try {
            this.conn = DriverManager.getConnection(url, username, password);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    private User findUser(Long aLong){
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"Users\"" +
                    "WHERE \"ID\"=" + "'" + aLong + "'");

            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                String password = resultSet.getString("Password");

                User usr = new User(firstName, lastName, email, password);
                usr.setId(id);

                return usr;
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return null;
    }

    @Override
    public Optional<Message> findOne(Long aLong){
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findOneStatement = conn.prepareStatement("SELECT * FROM \"Messages\"" +
                    "WHERE \"ID\"=" + "'" + aLong + "'");

            ResultSet resultSet = findOneStatement.executeQuery();

            if(resultSet.next()) {
                Long id = resultSet.getLong("ID");
                Long fromID = resultSet.getLong("FromID");
                String messg = resultSet.getString("Message");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();
                long replyID = resultSet.getLong("ReplyID");

                User fromUsr = this.findUser(fromID);
                ArrayList<User> toUsr = new ArrayList<>();

                PreparedStatement createList = conn.prepareStatement("SELECT \"UserID\" FROM " +
                        "\"MessageUsers\" WHERE \"MessageID\" = '" + id + "';");
                ResultSet usrs = createList.executeQuery();
                while (usrs.next()){
                    Long idU = usrs.getLong("UserID");
                    toUsr.add(this.findUser(idU));
                }


                Message msg;
                if(replyID == 0){
                    msg = new Message(fromUsr, toUsr, messg);
                }
                else {
                    msg = new ReplyMessage(fromUsr, toUsr, messg, this.findOne(replyID).get());
                }
                msg.setDate(time);
                msg.setId(id);

                return Optional.of(msg);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }


        return Optional.empty();
    }

    @Override
    public Iterable<Message> findAll() {
        HashSet<Message> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"Messages\"");

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("ID");
                Long fromID = resultSet.getLong("FromID");
                String messg = resultSet.getString("Message");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();
                long replyID = resultSet.getLong("ReplyID");

                User fromUsr = this.findUser(fromID);
                ArrayList<User> toUsr = new ArrayList<>();

                PreparedStatement createList = conn.prepareStatement("SELECT \"UserID\" FROM " +
                        "\"MessageUsers\" WHERE \"MessageID\" = '" + id + "';");

                ResultSet usrs = createList.executeQuery();
                while (usrs.next()){
                    Long idU = usrs.getLong("UserID");
                    toUsr.add(this.findUser(idU));
                }


                Message msg;
                if(replyID == 0){
                    msg = new Message(fromUsr, toUsr, messg);
                }
                else {
                    msg = new ReplyMessage(fromUsr, toUsr, messg, this.findOne(replyID).get());
                }
                msg.setDate(time);
                msg.setId(id);
                toReturn.add(msg);
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public Optional<Message> save(Message entity) {
        validator.validate(entity);

        try {
            String[] time = entity.getDate().toString().split("T");

            PreparedStatement statement;

            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            if(entity instanceof ReplyMessage){
                statement = conn.prepareStatement("INSERT INTO " +
                        "\"Messages\"(\"ID\", \"FromID\", \"Message\", \"Date\", \"ReplyID\") " +
                        "VALUES('" + entity.getId() + "','" + entity.getFrom().getId() + "','"
                        +  entity.getMessage() + "','" + time[0] + " " + time[1] + "','" +
                        ((ReplyMessage) entity).getMessageReply().getId() + "');");
            }

            else {
                statement = conn.prepareStatement("INSERT INTO " +
                        "\"Messages\"(\"ID\", \"FromID\", \"Message\", \"Date\") " +
                        "VALUES('" + entity.getId() + "','" + entity.getFrom().getId() + "','"
                         + entity.getMessage() + "','" + time[0] + " " + time[1]  + "');");

            }
            statement.execute();


            for(User u: entity.getTo()) {
                PreparedStatement relation = conn.prepareStatement("INSERT INTO" +
                        "\"MessageUsers\"(\"UserID\", \"MessageID\") " +
                        "VALUES ('" + u.getId() + "','" + entity.getId() +  "');");
                relation.execute();
            }

        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.of(entity);
        }

        return Optional.empty();
    }

    @Override
    public Optional<Message> delete(Long aLong) {
        Message msg = null;

        try {
            //check if user exists to be deleted, so deleted user can be returned
            Optional<Message> opt = this.findOne(aLong);

            if(opt.isPresent()){
                msg = opt.get();
            }
            else {
                return Optional.empty();
            }

            PreparedStatement statement2 = conn.prepareStatement("DELETE FROM " +
                    "\"Messages\"" +
                    "WHERE \"ID\" =" + "'" + aLong + "'");

            statement2.execute();

        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return Optional.ofNullable(msg);
    }

    @Override
    public Optional<Message> update(Message entity) {
        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement statement = conn.prepareStatement("UPDATE " +
                    "\"Messages\"" +
                    "SET \"Message\"='" + entity.getMessage() + "'" +
                    "WHERE \"ID\" =" + "'" + entity.getId() + "'");

            statement.execute();
        } catch (SQLException exc) {
            exc.printStackTrace();
            return Optional.ofNullable(entity);
        }


        return Optional.empty();
    }

    @Override
    public Page<Message> findAll(Pageable pageable) {
        Paginator<Message> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }

    @Override
    public Iterable<Message> findOnPage(int page) {
        HashSet<Message> toReturn = new HashSet<>();

        try {
            if(!conn.isValid(1)){
                this.conn = DriverManager.getConnection(url, username, password);
            }

            PreparedStatement findAllStatement = conn.prepareStatement("SELECT * FROM \"Messages\"" +
                    "ORDER BY \"ID\" OFFSET " + pageSize*page + " LIMIT " + pageSize);

            ResultSet resultSet = findAllStatement.executeQuery();

            while (resultSet.next()){
                Long id = resultSet.getLong("ID");
                Long fromID = resultSet.getLong("FromID");
                String messg = resultSet.getString("Message");
                LocalDateTime time = resultSet.getTimestamp("Date").toLocalDateTime();
                long replyID = resultSet.getLong("ReplyID");

                User fromUsr = this.findUser(fromID);
                ArrayList<User> toUsr = new ArrayList<>();

                PreparedStatement createList = conn.prepareStatement("SELECT \"UserID\" FROM " +
                        "\"MessageUsers\" WHERE \"MessageID\" = '" + id + "';");

                ResultSet usrs = createList.executeQuery();
                while (usrs.next()){
                    Long idU = usrs.getLong("UserID");
                    toUsr.add(this.findUser(idU));
                }


                Message msg;
                if(replyID == 0){
                    msg = new Message(fromUsr, toUsr, messg);
                }
                else {
                    msg = new ReplyMessage(fromUsr, toUsr, messg, this.findOne(replyID).get());
                }
                msg.setDate(time);
                msg.setId(id);
                toReturn.add(msg);
            }
        } catch (SQLException exc) {
            exc.printStackTrace();
        }

        return toReturn;
    }

    @Override
    public void setPageSize(int size) {
        this.pageSize = size;
    }

    @Override
    public Iterable<Message> findOnPageNon(int page, Long currentID){
        return new HashSet<>();
    }

    @Override
    public Iterable<Message> findOnPageFriends(int page, Long currentID){
        return new HashSet<>();
    }
}
