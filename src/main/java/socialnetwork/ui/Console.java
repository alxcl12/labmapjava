/*
 *  @author albua
 *  created on 24/10/2020
 */
package socialnetwork.ui;


import socialnetwork.domain.*;
import socialnetwork.domain.validators.MultipleUsersException;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * Basic console user interface
 */
public class Console {
    Service service;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public Console(Service serv){
        service = serv;
    }

    /**
     * Prints menu of the app
     */
    public void showMenu(){
        System.out.println("1. Add user");
        System.out.println("2. Delete user");
        System.out.println("3. Add friendship");
        System.out.println("4. Remove friendship");
        System.out.println("5. Print all users");
        System.out.println("6. Print all friendships");
        System.out.println("7. Show biggest community");
        System.out.println("8. Show numbers of communities");
        System.out.println("9. Show friendships by user and month");
        System.out.println("10. Show friendships by user");
        System.out.println("11. Go to user submenu");
        System.out.println("12. Show conversation of 2 users");
        System.out.println("13. Exit");
        System.out.print("Input: ");
    }

    /**
     * Prints user submenu of the app
     */
    public void showSubMenu(){
        System.out.println("1. Send friend request");
        System.out.println("2. Send message");
        System.out.println("3. Check messages");
        System.out.println("4. Back");
        System.out.print("Input: ");
    }

    /**
     * Collects data from user and adds an user
     * Handles exceptions
     * @throws IOException if IOException occurs while reading data
     */
    public void addUser() throws IOException {
        System.out.print("Input first name: ");
        String firstName = reader.readLine();

        System.out.print("Input last name: ");
        String lastName = reader.readLine();

        System.out.print("Input email: ");
        String email = reader.readLine();

        System.out.print("Input password: ");
        String password = reader.readLine();

        try{
            service.addUser(firstName, lastName, email, password);
            System.out.println("User added.\n");
        }
        catch (ValidationException | ServiceException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Prints all users
     */
    public void printAllUsers(){
        service.getAllUsers().forEach(System.out::println);
    }

    /**
     * Prints all friendships
     */
    public void printAllFriendship(){
        service.getAllFriendship().forEach(System.out::println);
    }

    /**
     * Collects data from user and deletes an user
     * Handles exceptions
     * @throws IOException if IOException occurs while reading data
     */
    public void deleteUser() throws IOException {
        System.out.print("User first name: ");
        String firstName = reader.readLine();

        System.out.print("User last name: ");
        String lastName = reader.readLine();

        try {
            service.removeUser(firstName, lastName);
            System.out.println("User deleted.\n");
        }
        catch (ValidationException | ServiceException e){
            System.out.println(e.getMessage());
        }
        catch (MultipleUsersException e){
            ArrayList<User> users = (ArrayList<User>) service.getAllByName(firstName, lastName);
            users.forEach(System.out::println);

            System.out.print("Multiple users found, select which ID to delete: ");
            String id = reader.readLine();

            try {
                Long.parseLong(id);
            }
            catch (NumberFormatException ex){
                System.out.println("Invalid ID!\n");
            }

            try {
                service.removeUserID(id);
                System.out.println("User deleted.\n");
            }
            catch (ServiceException ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Collects user input of 2 users
     * @return Tuple containing the two users, first name to the left, last name to the right
     * @throws IOException if IOException occurs while reading data
     */
    public Tuple<Tuple<String, String>, Tuple<String, String>> askNames() throws IOException {
        System.out.print("Input first name of one user: ");
        String firstName1 = reader.readLine();

        System.out.print("Input last name of one user: ");
        String lastName1 = reader.readLine();

        System.out.println("\n");

        System.out.print("Input first name of one user: ");
        String firstName2 = reader.readLine();

        System.out.print("Input last name of one user: ");
        String lastName2 = reader.readLine();

        Tuple<String, String> t1 = new Tuple<>(firstName1, lastName1);
        Tuple<String, String> t2 = new Tuple<>(firstName2, lastName2);

        return new Tuple<>(t1, t2);
    }

    /**
     * Collects data from user and adds a friendship
     * Handles exceptions
     * @throws IOException if IOException occurs while reading data
     */
    public void addFriendship() throws IOException {
        Tuple<Tuple<String, String>, Tuple<String, String>> names = this.askNames();

        String firstName1 = names.getLeft().getLeft();
        String lastName1 = names.getLeft().getRight();
        String firstName2 = names.getRight().getLeft();
        String lastName2 = names.getRight().getRight();

        try{
            service.addFriendship(firstName1, lastName1, firstName2, lastName2);
            System.out.println("Friendship added.\n");
        }
        catch (ValidationException | ServiceException e){
            System.out.println(e.getMessage());
        }
        catch (MultipleUsersException ex){
            Tuple<String, String> tuple = this.askIDDelete(firstName1,lastName1,firstName2,lastName2);
            String id1 = tuple.getLeft();
            String id2 = tuple.getRight();
            try {
                service.addFriendShipID(id1, id2);
                System.out.println("Friendship added.\n");
            }
            catch (ServiceException e){
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Method used if there are multiple users with given first names and last names while deleting friendship
     * Collects input from user as of which ID is meant in the operation
     * Return a tuple, containing IDs of entities
     * @param firstName1 first name of one user
     * @param lastName1 last name of one user
     * @param firstName2 first name of one user
     * @param lastName2 last name of one user
     * @return Tuple containing corresponding IDs
     * @throws IOException if exception occurs while collecting data
     */
    private Tuple<String, String> askIDDelete(String firstName1, String lastName1, String firstName2, String lastName2) throws IOException {
        ArrayList<User> users1 = (ArrayList<User>) service.getAllByName(firstName1, lastName1);
        users1.forEach(System.out::println);

        System.out.println("\n");

        ArrayList<User> users2 = (ArrayList<User>) service.getAllByName(firstName2, lastName2);
        users2.forEach(System.out::println);

        System.out.println("\n");

        System.out.println("Multiple users found, select ID of each user");

        System.out.print("ID1: ");
        String id1 = reader.readLine();

        System.out.print("ID2: ");
        String id2 = reader.readLine();

        try {
            Long.parseLong(id1);
            Long.parseLong(id2);
        }
        catch (NumberFormatException exc){
            System.out.println("Invalid ID!\n");
        }

        return new Tuple<>(id1, id2);
    }

    /**
     * Method used if there are multiple users with given first name and last name while showing reports
     * @param firstName first name of user
     * @param lastName last name of user
     * @return ID of user
     * @throws IOException if exception occurs while collecting data
     */
    private Long askIDReport(String firstName, String lastName) throws IOException {
        ArrayList<User> users = (ArrayList<User>) service.getAllByName(firstName, lastName);
        users.forEach(System.out::println);

        System.out.print("Multiple users found, select which ID to display friends: ");
        String id = reader.readLine();
        System.out.println();

        try {
            Long.parseLong(id);
        }
        catch (NumberFormatException ex){
            System.out.println("Invalid ID!\n");
        }

        return Long.parseLong(id);
    }

    /**
     * Collects data from user and deletes a friendship
     * Handles exceptions
     * @throws IOException if IOException occurs while reading data
     */
    public void deleteFriendship() throws IOException {
        System.out.print("Input first name of one user to be deleted: ");
        String firstName1 = reader.readLine();

        System.out.print("Input last name of one user user to be deleted: ");
        String lastName1 = reader.readLine();

        System.out.println("\n");

        System.out.print("Input first name of one user user to be deleted: ");
        String firstName2 = reader.readLine();

        System.out.print("Input last name of one user user to be deleted: ");
        String lastName2 = reader.readLine();

        try{
            service.removeFriendship(firstName1, lastName1, firstName2, lastName2);
            System.out.println("Friendship deleted.\n");
        }
        catch (ValidationException e){
            System.out.println(e.getMessage());
        }
        catch (MultipleUsersException ex){
            Tuple<String, String> tuple = this.askIDDelete(firstName1,lastName1,firstName2,lastName2);
            String id1 = tuple.getLeft();
            String id2 = tuple.getRight();

            try {
                Long.parseLong(id1);
                Long.parseLong(id2);
            }
            catch (NumberFormatException exc){
                System.out.println("Invalid ID!\n");
            }

            service.removeFriendshipID(id1, id2);
            System.out.println("Friendship deleted.\n");
        }
    }

    /**
     * Prints largest community of users
     */
    public void getBiggestCommunity(){
        ArrayList<User> list = service.getMostSociable();

        list.forEach(System.out::println);
        System.out.println("\n");
    }

    /**
     * Prints number of social communities
     */
    public void getNrCommunities(){
        System.out.println(service.getNrCommunities());
        System.out.println("\n");
    }

    /**
     * Collects data from user and prints all friends of input user started in input month
     * @throws IOException if IOException occurs while reading data
     */
    public void getFriendsByMonth() throws IOException {
        System.out.print("Input first name: ");
        String firstName = reader.readLine();

        System.out.print("Input last name: ");
        String lastName = reader.readLine();

        System.out.print("Input month(numeric): ");
        String month = reader.readLine();

        long monthLong;
        try {
            monthLong = Long.parseLong(month);
            if(!(monthLong >= 1 && monthLong <=12)){
                throw new NumberFormatException();
            }
        }
        catch (NumberFormatException e){
            System.out.println("Invalid month!\n");
            return;
        }

        try {
            Iterable<FriendDTO> data = service.getFriendsByMonth(firstName, lastName, monthLong);
            FriendDTO.printHeader();
            data.forEach(FriendDTO::printFormatted);
            System.out.println();
        }
        catch (ValidationException e){
            System.out.println(e.getMessage());
        }
        catch (MultipleUsersException exc){
            Long id = this.askIDReport(firstName, lastName);

            Iterable<FriendDTO> data = service.getFriendsByMonthID(id, monthLong);

            FriendDTO.printHeader();
            data.forEach(FriendDTO::printFormatted);
            System.out.println();
        }
    }

    /**
     * Collects data from user and prints all friends of input user
     * @throws IOException if IOException occurs while reading data
     */
    public void getFriends() throws IOException {
        System.out.print("Input first name: ");
        String firstName = reader.readLine();

        System.out.print("Input last name: ");
        String lastName = reader.readLine();

        try{
            Iterable<FriendDTO> data = service.getFriends(firstName, lastName);
            FriendDTO.printHeader();
            data.forEach(FriendDTO::printFormatted);
            System.out.println();
        }
        catch (ValidationException e){
            System.out.println(e.getMessage());
        }
        catch (MultipleUsersException exc){
            Long id = this.askIDReport(firstName, lastName);

            Iterable<FriendDTO> data = service.getFriendsID(id);

            FriendDTO.printHeader();
            data.forEach(FriendDTO::printFormatted);
            System.out.println();
        }
    }

    /**
     * Displays conversation between two users read from input
     * @throws IOException if IOException occurs while reading data
     */
    public void getMessagesUsers() throws IOException {
        Tuple<Tuple<String, String>, Tuple<String, String>> names = this.askNames();
        String firstName1 = names.getLeft().getLeft();
        String lastName1 = names.getLeft().getRight();
        String firstName2 = names.getRight().getLeft();
        String lastName2 = names.getRight().getRight();

        try{
            service.getMessagesUsers(firstName1, lastName1, firstName2, lastName2)
                    .forEach(System.out::println);
        }
        catch (ValidationException | ServiceException e){
            System.out.println(e.getMessage());
        }
        catch (MultipleUsersException ex){
            Tuple<String, String> tuple = this.askIDDelete(firstName1,lastName1,firstName2,lastName2);

            String id1 = tuple.getLeft();
            String id2 = tuple.getRight();
            try {
                Long.parseLong(id1);
                Long.parseLong(id2);
            }
            catch (NumberFormatException e){
                System.out.println("Invalid IDs!\n");
                return;
            }

            try {
                service.getMessagesUsersID(Long.parseLong(id1), Long.parseLong(id2))
                        .forEach(System.out::println);
            }
            catch (ServiceException e){
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Runner of the app
     * @throws IOException if IOException occurs while reading data
     */
    public void run() throws IOException {
        while(true){
            showMenu();
            int next;
            try {
                next = Integer.parseInt(reader.readLine());
            }
            catch (NumberFormatException e){
                System.out.println("Invalid command!\n");
                continue;
            }

            if(next == 1){
                addUser();
            }
            else if (next == 2){
                deleteUser();
            }
            else if (next == 3) {
                addFriendship();
            }
            else if (next == 4){
                deleteFriendship();
            }
            else if (next == 5){
                printAllUsers();
            }
            else if (next == 6){
                printAllFriendship();
            }
            else if (next == 7){
                getBiggestCommunity();
            }
            else if (next == 8){
                getNrCommunities();
            }
            else if (next == 9){
                getFriendsByMonth();
            }
            else if (next == 10){
                getFriends();
            }
            else if (next == 11){
                runSubMenu();
            }
            else if (next == 12){
                getMessagesUsers();
            }
            else {
                break;
            }
        }
    }

    /**
     * Collects data from user as of which friend to send a request and sends it
     * @param currentID ID of current logged user
     * @throws IOException if IOException occurs while collecting data
     */
    public void sendFriendRequest(Long currentID) throws IOException {
        System.out.print("Input first name of user: ");
        String firstName = reader.readLine();

        System.out.print("Input last name of user: ");
        String lastName = reader.readLine();

        System.out.println("\n");

        try{
            service.addFriendRequest(firstName, lastName, currentID);
            System.out.println("Friend request sent.\n");
        }
        catch (ValidationException | ServiceException e){
            System.out.println(e.getMessage());
        }
        catch (MultipleUsersException ex){
            Long otherID = this.askIDReport(firstName, lastName);
            try {
                service.addFriendRequestID(otherID, currentID);
                System.out.println("Friend request sent.\n");
            }
            catch (ServiceException e){
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Sends message to another user collected from input
     * @param currentID id of current logged in user
     * @throws IOException if error occurs while collecting data
     */
    public void sendMessage(Long currentID) throws IOException {
        System.out.print("Input how many users to send message: ");
        String nrString = reader.readLine();
        try {
            Long.parseLong(nrString);
        }
        catch (NumberFormatException e){
            System.out.println("Invalid number!\n");
            return;
        }

        System.out.print("Message: ");
        String message= reader.readLine();
        String lastName, firstName;
        List<Long> idList = new ArrayList<>();

        for(int i = 0; i < Long.parseLong(nrString); i++) {

            System.out.print("Input first name of user to send message: ");
            firstName = reader.readLine();

            System.out.print("Input last name of user to send message: ");
            lastName = reader.readLine();

            ArrayList<User> current = (ArrayList<User>) service.getAllByName(firstName, lastName);
            if(current.size() == 1){
                idList.add(current.get(0).getId());
            }
            else if (current.size() > 1){
                idList.add(this.askIDReport(firstName, lastName));
            }

            System.out.println("\n");
        }

        try{
            service.sendMessage(idList, currentID, message);
            System.out.println("Message sent.\n");
        }
        catch (ValidationException | ServiceException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Logs in one user(no password yet) and returns its ID
     * @return Optional containing ID of user or empty if user does not exist
     * @throws IOException if IOException occurs while collecting data
     */
    private Optional<Long> login() throws IOException {
        System.out.println("Who is this?");
        System.out.print("Input first name of user: ");
        String firstName = reader.readLine();

        System.out.print("Input last name of user: ");
        String lastName = reader.readLine();

        Long currentID;
        ArrayList<User> usr = (ArrayList<User>) service.getAllByName(firstName, lastName);

        if(usr.size() == 1){
            currentID = usr.get(0).getId();
        }
        else if (usr.size() == 0){
            System.out.println("User does not exist!\n");
            return Optional.empty();
        }
        else {
            usr.forEach(System.out::println);

            System.out.println("\n");

            System.out.println("Multiple users found, select ID");

            System.out.print("ID: ");
            String id1 = reader.readLine();
            try {
                currentID = Long.parseLong(id1);
            }
            catch (NumberFormatException e){
                System.out.println("Invalid ID!\n");
                return Optional.empty();
            }
        }
        return Optional.ofNullable(currentID);
    }

    /**
     * Manages friend requests for current logged in user
     * Asks user if friend is to be added or not and performs operation
     * @param currentID ID of logged in user
     */
    private void manageRequests(Long currentID){
        Iterable<FriendRequest> requests = service.getRequests(currentID);
        StreamSupport.stream(requests.spliterator(), false)
                .forEach(x->{
                    if(x.getStatus().equals("Pending")){
                        System.out.println("New friend request from " +
                                service.getUser(x.getRequesterID()).getFirstName() + " " +
                                service.getUser(x.getRequesterID()).getLastName());

                        System.out.println("1. Accept");
                        System.out.println("2. Refuse");
                        System.out.print("Input: ");
                        String input;
                        try {
                            input = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                            return;
                        }
                        long action = 0;
                        try {
                            action = Long.parseLong(input);
                        }
                        catch (NumberFormatException e){
                            System.out.println("Invalid command!\n");
                        }
                        if(action == 1){
                            service.acceptFriendRequest(x);
                            service.addFriendShipID(x.getRequesterID().toString(), x.getReceiverID().toString());
                            System.out.println("Friend request accepted!\n");
                        }
                        else if(action == 2){
                            service.denyFriendRequest(x);
                            System.out.println("Friend request denied!\n");
                        }
                        System.out.println("\n");
                    }
                });
    }

    /**
     * Checks if current user has any unseen messages and displays them
     * Gives to user the possibility to reply to any of those messages or continue
     * @param currentID id of current logged in user
     */
    private void checkMessages(Long currentID){
        Iterable<Message> messages = service.getMessagesUser(currentID);
        StreamSupport.stream(messages.spliterator(), false)
                .forEach(x->{
                    System.out.println("ID: " + x.getId() + " From: " + x.getFrom() + " Message: " + x.getMessage());
                });
        System.out.println("1. Select message to reply");
        System.out.println("2. Back");
        System.out.print("Input: ");
        String input;

        try {
            input = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        long action = 0;
        try {
            action = Long.parseLong(input);
        }
        catch (NumberFormatException e){
            System.out.println("Invalid command!\n");
        }

        if(action == 1){
            System.out.print("Select ID of message to reply: ");
            try {
                input = reader.readLine();
            }
            catch (IOException e){
                System.out.println("Unexpected error!\n");
            }

            try {
                action = Long.parseLong(input);
            }
            catch (NumberFormatException e){
                System.out.println("ID must be numeric!\n");
            }

            long finalAction = action;
            StreamSupport.stream(messages.spliterator(), false)
                    .filter(x-> x.getId().equals(finalAction))
                    .forEach(x->{
                        System.out.print("Message: ");

                        String msg;
                        try {
                            msg = reader.readLine();
                        } catch (IOException e) {
                            System.out.println("Unexpected error!\n");
                            return;
                        }

                        try{
                            ArrayList<Long> param = new ArrayList<>();
                            param.add(x.getFrom().getId());
                            service.sendReply(param, currentID, msg, x);
                            System.out.println("Reply sent.\n");
                        }
                        catch (ValidationException | ServiceException e){
                            System.out.println(e.getMessage());
                        }
                    });
        }
        else if(action == 2){
            return;
        }
        System.out.println("\n");
    }

    /**
     * Runner of sub menu managing user logins, requests and messages
     * @throws IOException if IOException occurs while collecting data
     */
    public void runSubMenu() throws IOException{
        Optional<Long> optID = this.login();
        Long currentID;
        if(optID.isPresent()){
            currentID = optID.get();
        }
        else {
            return;
        }

        this.manageRequests(currentID);

        while (true){
            showSubMenu();
            int next;
            try {
                next = Integer.parseInt(reader.readLine());
            }
            catch (NumberFormatException e){
                System.out.println("Invalid command!\n");
                continue;
            }

            if(next == 1){
                sendFriendRequest(currentID);
            }
            else if (next == 2){
                sendMessage(currentID);
            }
            else if (next == 3){
                checkMessages(currentID);
            }
            else {
                break;
            }
        }
    }
}
