/*
 *  @author albua
 *  created on 19/11/2020
 */
package socialnetwork.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;

import java.io.File;
import java.net.MalformedURLException;

/**
 * Helper class to display an error or information window
 */
public class MessageAlert {
    /**
     * Displays a window
     * @param owner owner of window
     * @param type alert type
     * @param header header of window
     * @param text message to display
     */
    static void showMessage(Stage owner, Alert.AlertType type, String header, String text){
        Alert message=new Alert(type);
        message.setHeaderText(header);
        message.setContentText(text);
        message.initOwner(owner);

        message.initModality(Modality.APPLICATION_MODAL);

        DialogPane dialogPane = message.getDialogPane();
        String str = fileToStylesheetString(new File("C:\\Facultate\\New\\AnulII\\SemestrulIII\\" +
                "Metodeavansatedeprogramare\\Laboratoare\\LabJava\\src\\main\\resources\\css\\myDialog.css"));
        dialogPane.getStylesheets().add(str);
        dialogPane.getStyleClass().add("myDialog");
        message.show();
    }

    /**
     * Displays error window
     * @param owner owner of window
     * @param text message of error
     */
    static void showErrorMessage(Stage owner, String text){
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.initOwner(owner);
        message.setTitle("Error");
        message.setContentText(text);

        DialogPane dialogPane = message.getDialogPane();
        String str = fileToStylesheetString(new File("C:\\Facultate\\New\\AnulII\\SemestrulIII\\" +
                "Metodeavansatedeprogramare\\Laboratoare\\LabJava\\src\\main\\resources\\css\\myDialog.css"));
        dialogPane.getStylesheets().add(str);
        dialogPane.getStyleClass().add("myDialog");
        message.showAndWait();
    }

    static String fileToStylesheetString ( File stylesheetFile ) {
        try {
            return stylesheetFile.toURI().toURL().toString();
        } catch ( MalformedURLException e ) {
            return null;
        }
    }
}
