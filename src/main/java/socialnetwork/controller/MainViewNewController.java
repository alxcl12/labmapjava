/*
 *  @author albua
 *  created on 02/12/2020
 */
package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.fxml.FXML;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ScrollEvent;
import javafx.stage.DirectoryChooser;
import javafx.util.converter.LocalTimeStringConverter;
import socialnetwork.domain.*;
import socialnetwork.domain.Event;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.Service;
import socialnetwork.utils.Observer;
import socialnetwork.utils.ReportGenerator;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Controller class for the main window
 * Implements Observer interface, observes the service
 */
public class MainViewNewController implements Observer {
    private Service service;

    //id of logged in user of session
    private Long currentId;

    //models
    ObservableList<User> modelMain = FXCollections.observableArrayList();
    ObservableList<FriendRequestDTO> modelReq = FXCollections.observableArrayList();
    ObservableList<Message> modelMes = FXCollections.observableArrayList();
    ObservableList<Message> modelMesSent = FXCollections.observableArrayList();
    ObservableList<Message> modelMesRep = FXCollections.observableArrayList();
    ObservableList<User> modelNon = FXCollections.observableArrayList();
    ObservableList<FriendRequestDTO> modelReqPage = FXCollections.observableArrayList();
    ObservableList<FriendRequestDTO> modelFriendRep = FXCollections.observableArrayList();
    ObservableList<Message> modelMesPage = FXCollections.observableArrayList();
    ObservableList<User> modelNonPage = FXCollections.observableArrayList();
    ObservableList<Event> modelRegistered = FXCollections.observableArrayList();
    ObservableList<Event> modelUnregistered = FXCollections.observableArrayList();
    ObservableList<User> modelParticipants = FXCollections.observableArrayList();

    @FXML
    Label sessionLabel;
    @FXML
    Button logoutButton;
    @FXML
    TextField searchBox;
    @FXML
    TextField filterFriends;
    @FXML
    TextField eventName;
    @FXML
    TextField eventDesc;
    @FXML
    TextArea textAreaMes;
    @FXML
    TextArea textAreaPage;
    @FXML
    Label firstLabel;
    @FXML
    Label lastLabel;
    @FXML
    Spinner<LocalTime> spinner;

    //table columns
    @FXML
    TableColumn<User, String> tableColumnID;
    @FXML
    TableColumn<User, String> tableColumnFirst;
    @FXML
    TableColumn<User, String> tableColumnLast;
    @FXML
    TableColumn<User, String> tableColumnEmail;

    @FXML
    TableColumn<User, String> tableColumnIDParticipant;
    @FXML
    TableColumn<User, String> tableColumnFirstParticipant;
    @FXML
    TableColumn<User, String> tableColumnLastParticipant;
    @FXML
    TableColumn<User, String> tableColumnEmailParticipant;

    @FXML
    TableColumn<User, String> tableColumnIDNon;
    @FXML
    TableColumn<User, String> tableColumnFirstNon;
    @FXML
    TableColumn<User, String> tableColumnLastNon;
    @FXML
    TableColumn<User, String> tableColumnEmailNon;

    @FXML
    TableColumn<User, String> tableColumnFirstNonPage;
    @FXML
    TableColumn<User, String> tableColumnLastNonPage;
    @FXML
    TableColumn<User, String> tableColumnEmailNonPage;
    @FXML
    TableColumn<User, String> tableColumnIDNonPage;

    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnStatusReq;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnFirstReq;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnLastReq;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnDateReq;

    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnFirstRep;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnLastRep;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnDateFriendRep;

    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnStatusReqPage;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnFirstReqPage;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnLastReqPage;
    @FXML
    TableColumn<FriendRequestDTO, String> tableColumnDateReqPage;

    @FXML
    TableColumn<Message, String> tableColumnMesMes;
    @FXML
    TableColumn<User, String> tableColumnFromMes;
    @FXML
    TableColumn<Message, String> tableColumnDateMes;

    @FXML
    TableColumn<Message, String> tableColumnMesRep;
    @FXML
    TableColumn<User, String> tableColumnFromRep;
    @FXML
    TableColumn<Message, String> tableColumnDateMesRep;

    @FXML
    TableColumn<Message, String> tableColumnMesMesSent;
    @FXML
    TableColumn<User, String> tableColumnToMesSent;
    @FXML
    TableColumn<Message, String> tableColumnDateMesSent;

    @FXML
    TableColumn<Message, String> tableColumnMesMesPage;
    @FXML
    TableColumn<User, String> tableColumnFromMesPage;
    @FXML
    TableColumn<Message, String> tableColumnDateMesPage;

    @FXML
    TableColumn<Event, String> tableColumnNameUnregistered;
    @FXML
    TableColumn<Event, String> tableColumnDescUnregistered;
    @FXML
    TableColumn<Event, String> tableColumnDateUnregistered;

    @FXML
    TableColumn<Event, String> tableColumnNameRegistered;
    @FXML
    TableColumn<Event, String> tableColumnDescRegistered;
    @FXML
    TableColumn<Event, String> tableColumnDateRegistered;

    //table views
    @FXML
    TableView<User> tableViewMain;
    @FXML
    TableView<User> tableViewParticipants;
    @FXML
    TableView<FriendRequestDTO> tableViewReq;
    @FXML
    TableView<FriendRequestDTO> tableViewFriendsRep;
    @FXML
    TableView<Message> tableViewMes;
    @FXML
    TableView<Message> tableViewMesRep;
    @FXML
    TableView<User> tableViewNonFriends;
    @FXML
    TableView<User> tableViewNonPage;
    @FXML
    TableView<FriendRequestDTO> tableViewReqPage;
    @FXML
    TableView<Message> tableViewMesPage;
    @FXML
    TableView<Message> tableViewMesSent;
    @FXML
    TableView<Event> tableViewRegistered;
    @FXML
    TableView<Event> tableViewUnregistered;

    //tabs
    @FXML
    TabPane tabPane;
    @FXML
    Tab friendRequestTab;
    @FXML
    Tab messagesTab;
    @FXML
    Tab reportsTab;
    @FXML
    Tab myPageTab;
    @FXML
    Tab eventsTab;

    //reports tab
    @FXML
    CheckBox checkBoxReport1;
    @FXML
    CheckBox checkBoxReport2;
    @FXML
    TextField outputTextField;
    @FXML
    TextField nameReport;
    @FXML
    DatePicker startDate;
    @FXML
    DatePicker endDate;
    @FXML
    DatePicker dateEvent;


    /**
     * Initializes table columns for User entity
     * @param tableColumnEmail email column
     * @param tableColumnID id column
     * @param tableColumnFirst first name column
     * @param tableColumnLast last name column
     */
    private void initializeUserTable(TableColumn<User, String> tableColumnEmail,
                                     TableColumn<User, String> tableColumnID, TableColumn<User, String> tableColumnFirst,
                                     TableColumn<User, String> tableColumnLast)
    {
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<>("Email"));
        tableColumnID.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnFirst.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnLast.setCellValueFactory(new PropertyValueFactory<>("lastName"));
    }

    /**
     * Initializes table columns for FriendRequestDTO entity
     * @param tableColumnFirst first name column
     * @param tableColumnLast last name column
     * @param tableColumnDate date column
     * @param tableColumnStatus status column
     */
    private void initializeRequestTable(TableColumn<FriendRequestDTO, String> tableColumnFirst,
                                        TableColumn<FriendRequestDTO, String> tableColumnLast,
                                        TableColumn<FriendRequestDTO, String> tableColumnDate,
                                        TableColumn<FriendRequestDTO, String> tableColumnStatus)
    {
        tableColumnStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        tableColumnFirst.setCellValueFactory(new PropertyValueFactory<>("firstNameFriend"));
        tableColumnLast.setCellValueFactory(new PropertyValueFactory<>("lastNameFriend"));
    }

    /**
     * Initializes table columns for Message entity
     * @param tableColumnDate date column
     * @param tableColumnFrom from column(user name)
     * @param tableColumnMes message column
     */
    private void initializeMessageTable(TableColumn<Message, String> tableColumnDate,
                                        TableColumn<User, String> tableColumnFrom,
                                        TableColumn<Message, String> tableColumnMes)
    {
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("dateFormat"));
        tableColumnMes.setCellValueFactory(new PropertyValueFactory<>("message"));
        tableColumnFrom.setCellValueFactory(new PropertyValueFactory<>("from"));

    }

    /**
     * Initializes table columns for Event entity
     * @param tableColumnName name column
     * @param tableColumnDesc description column
     * @param tableColumnDate date column
     */
    private void initializeEventTable(TableColumn<Event, String> tableColumnName,
                                      TableColumn<Event, String> tableColumnDesc,
                                      TableColumn<Event, String> tableColumnDate)
    {
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("dateFormat"));
        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableColumnDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
    }

    /**
     * Initializes MyPage tab
     */
    private void initializePage(){
        initializeUserTable(tableColumnEmailNonPage, tableColumnIDNonPage, tableColumnFirstNonPage, tableColumnLastNonPage);
        tableViewNonPage.setItems(modelNonPage);

        initializeRequestTable(tableColumnFirstReqPage, tableColumnLastReqPage, tableColumnDateReqPage, tableColumnStatusReqPage);
        tableViewReqPage.setItems(modelReqPage);

        initializeMessageTable(tableColumnDateMesPage, tableColumnFromMesPage, tableColumnMesMesPage);
        tableViewMesPage.setItems(modelMesPage);

        initializeEventTable(tableColumnNameRegistered, tableColumnDescRegistered, tableColumnDateRegistered);
        tableViewRegistered.setItems(modelRegistered);

        initializeEventTable(tableColumnNameUnregistered, tableColumnDescUnregistered, tableColumnDateUnregistered);
        tableViewUnregistered.setItems(modelUnregistered);
    }

    /**
     * Sets labels for all tables when no data is to be displayed
     */
    private void initializeLabelsTables(){
        tableViewParticipants.setPlaceholder(new Label("No participants or no event selected!"));
        tableViewRegistered.setPlaceholder(new Label("No events followed!"));
        tableViewMain.setPlaceholder(new Label("No friends!"));
        tableViewUnregistered.setPlaceholder(new Label("No events available!"));
        tableViewMes.setPlaceholder(new Label("No sent messages!"));
        tableViewMesSent.setPlaceholder(new Label("No received messages!"));
        tableViewReq.setPlaceholder(new Label("No requests!"));
        tableViewMesRep.setPlaceholder(new Label("Generate report in order to display messages"));
        tableViewFriendsRep.setPlaceholder(new Label("Generate report in order to display friends"));
        tableViewReqPage.setPlaceholder(new Label("No requests!"));
        tableViewMesPage.setPlaceholder(new Label("No received messages!"));
    }

    /**
     * Initializes all window and adds listeners
     */
    public void initialize() {
        initializeLabelsTables();

        initializeUserTable(tableColumnEmail, tableColumnID, tableColumnFirst, tableColumnLast);
        tableViewMain.setItems(modelMain);

        initializeUserTable(tableColumnEmailNon, tableColumnIDNon, tableColumnFirstNon, tableColumnLastNon);
        tableViewNonFriends.setItems(modelNon);

        initializeRequestTable(tableColumnFirstReq, tableColumnLastReq, tableColumnDateReq, tableColumnStatusReq);
        tableViewReq.setItems(modelReq);

        initializeMessageTable(tableColumnDateMes, tableColumnFromMes, tableColumnMesMes);
        tableViewMes.setItems(modelMes);

        initializeMessageTable(tableColumnDateMesSent, tableColumnToMesSent, tableColumnMesMesSent);
        tableColumnToMesSent.setCellValueFactory(new PropertyValueFactory<>("toFormat"));
        tableViewMesSent.setItems(modelMesSent);

        initializeMessageTable(tableColumnDateMesRep, tableColumnFromRep, tableColumnMesRep);
        tableViewMesRep.setItems(modelMesRep);

        initializeUserTable(tableColumnEmailParticipant, tableColumnIDParticipant, tableColumnFirstParticipant, tableColumnLastParticipant);
        tableViewParticipants.setItems(modelParticipants);

        tableColumnDateFriendRep.setCellValueFactory(new PropertyValueFactory<>("date"));
        tableColumnFirstRep.setCellValueFactory(new PropertyValueFactory<>("firstNameFriend"));
        tableColumnLastRep.setCellValueFactory(new PropertyValueFactory<>("lastNameFriend"));
        tableViewFriendsRep.setItems(modelFriendRep);

        initializePage();

        searchBox.textProperty().addListener((observable, oldValue, newValue) ->
                tableViewNonFriends
                                .setItems(filteredUsers(StreamSupport
                                .stream(service.getAllNonFriends(currentId).spliterator(), false)
                                .collect(Collectors.toList()), searchBox)));

        filterFriends.textProperty().addListener((observable, oldValue, newValue) ->
                tableViewMain
                        .setItems(filteredUsers(StreamSupport
                                .stream(service.getFriendsList(currentId).spliterator(), false)
                                .collect(Collectors.toList()), filterFriends)));

        //adding listeners
        cancelSelectionTables(tableViewMain, tableViewMes);
        cancelSelectionTables(tableViewMain, tableViewMesPage);
        cancelSelectionTables(tableViewUnregistered, tableViewRegistered);
        cancelSelectionTables(tableViewMes, tableViewMesSent);

        checkBoxReport1.selectedProperty().addListener((obs, oldSelection, newSelection) ->{
            checkBoxReport2.setSelected(false);
            tableViewMain.getSelectionModel().clearSelection();
        });

        checkBoxReport2.selectedProperty().addListener((obs, oldSelection, newSelection) -> checkBoxReport1.setSelected(false));

        addListenerPageNon(tableViewNonFriends, modelNon);
        addListenerPageNon(tableViewNonPage, modelNonPage);

        addListenerPageFriends(tableViewMain, modelMain);

        addListenerPageMessage(tableViewMes, modelMes);
        addListenerPageMessage(tableViewMesPage, modelMesPage);
        addListenerMessageSent(tableViewMesSent, modelMesSent);

        addListenerPageRequests(tableViewReq, modelReq);
        addListenerPageRequests(tableViewReqPage, modelReqPage);

        addListenerEventParticipant(tableViewRegistered);
        addListenerEventParticipant(tableViewUnregistered);

        setSpinnerFactory();
    }

    /**
     * Listener to display into participants table, all participants of selected event
     * @param tableViewEvent table view for Event
     */
    private void addListenerEventParticipant(TableView<Event> tableViewEvent){
        tableViewEvent.setOnMouseClicked(event -> {
            Event ev = tableViewEvent.getSelectionModel().getSelectedItem();
            if(ev != null){
                modelParticipants.clear();
                modelParticipants.addAll(ev.getParticipants());
            }
        });
    }

    /**
     * Listener to get more data to display(paging) when scrolled
     * @param tableViewReqPage table view containing requests
     * @param modelReqPage model where data is stored
     */
    private void addListenerPageRequests(TableView<FriendRequestDTO> tableViewReqPage, ObservableList<FriendRequestDTO> modelReqPage) {
        tableViewReqPage.addEventFilter(ScrollEvent.ANY, event -> {
            Iterable<FriendRequest> requests;
            requests = service.getNextReq(currentId);

            ArrayList<FriendRequestDTO> toAdd = filterRequests(requests);

            modelReqPage.addAll(toAdd);
        });
    }

    /**
     * Listener to get more data to display(paging) when scrolled
     * @param tableViewMes table view containing received messages
     * @param modelMes model where data is stored
     */
    private void addListenerPageMessage(TableView<Message> tableViewMes, ObservableList<Message> modelMes) {
        tableViewMes.addEventFilter(ScrollEvent.ANY, event -> {
            Iterable<Message> messages;
            messages = service.getNextMes(currentId);

            List<Message> messageList = StreamSupport.stream(messages.spliterator(), false)
                    .collect(Collectors.toList());

            modelMes.addAll(messageList);
        });
    }

    /**
     * Listener to get more data to display(paging) when scrolled
     * @param tableViewMes1 table view containing sent messages
     * @param modelMes1 model where data is stored
     */
    private void addListenerMessageSent(TableView<Message> tableViewMes1, ObservableList<Message> modelMes1) {
        tableViewMes1.addEventFilter(ScrollEvent.ANY, event -> {
            Iterable<Message> messages;

            messages = service.getNextMesSent(currentId);

            List<Message> messageList = StreamSupport.stream(messages.spliterator(), false)
                    .collect(Collectors.toList());

            modelMes1.addAll(messageList);
        });
    }

    /**
     * Listener to get more data to display(paging) when scrolled
     * @param tableViewNonFriends table view containing non friends
     * @param modelNon model where data is stored
     */
    private void addListenerPageNon(TableView<User> tableViewNonFriends, ObservableList<User> modelNon) {
        tableViewNonFriends.addEventFilter(ScrollEvent.ANY, event -> {
            Iterable<User> usersNon;
            usersNon = service.getNextNonFriends(currentId);

            List<User> userListNon = StreamSupport.stream(usersNon.spliterator(), false)
                    .collect(Collectors.toList());

            modelNon.addAll(userListNon);
        });
    }

    /**
     * Listener to get more data to display(paging) when scrolled
     * @param tableView table view containing friends
     * @param model model where data is stored
     */
    private void addListenerPageFriends(TableView<User> tableView, ObservableList<User> model){
        tableView.addEventFilter(ScrollEvent.ANY, event -> {
            Iterable<User> users;
            users = service.getNextFriends(currentId);

            List<User> userList = StreamSupport.stream(users.spliterator(), false)
                    .collect(Collectors.toList());

            model.addAll(userList);
        });
    }

    /**
     * Cancels out selection out of 2 table views. If something is selected in one tables,
     * deselect in the other one
     * @param tableView1 one table view
     * @param tableView2 second table view
     */
    private void cancelSelectionTables(TableView<?> tableView1, TableView<?> tableView2){
        tableView2.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection)->{
            if(newSelection != null){
                tableView1.getSelectionModel().clearSelection();
            }
        });

        tableView1.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) ->{
            if(newSelection != null){
                tableView2.getSelectionModel().clearSelection();
            }
        });
    }

    /**
     * Sets spinner factory for spinner box
     * Displays hours and minutes
     */
    private void setSpinnerFactory(){
        SpinnerValueFactory<LocalTime> value = new SpinnerValueFactory<LocalTime>() {
            {
                setConverter(new LocalTimeStringConverter(FormatStyle.SHORT));
            }
            @Override
            public void decrement(int steps) {
                if(getValue()==null){
                    setValue(LocalTime.now());
                }
                else {
                    LocalTime time = getValue();
                    setValue(time.minusMinutes(steps));
                }
            }

            @Override
            public void increment(int steps) {
                if (this.getValue() == null)
                    setValue(LocalTime.now());
                else {
                    LocalTime time =  getValue();
                    setValue(time.plusMinutes(steps));
                }
            }
        };

        spinner.setValueFactory(value);
        spinner.setEditable(true);
    }

    /**
     * When services notifies, the table view is reloaded
     */
    @Override
    public void update() {
        initModel();
    }

    /**
     * Loads data into main table view depending on selected tab
     */
    private void initModel(){
        if(tabPane.getSelectionModel().getSelectedItem().equals(messagesTab)){
            loadMessagesTab();
        }
        else if(tabPane.getSelectionModel().getSelectedItem().equals(friendRequestTab)){
            loadRequestTab();
        }
        else if(tabPane.getSelectionModel().getSelectedItem().equals(reportsTab)){
            loadReportsTab();
        }
        else if(tabPane.getSelectionModel().getSelectedItem().equals(myPageTab)){
            loadMyPageTab();
        }
        else if(tabPane.getSelectionModel().getSelectedItem().equals(eventsTab)){
            loadEventsTab();
        }
    }

    /**
     * Loads data corresponding to reports tab
     */
    private void loadReportsTab(){
        nameReport.setText("Generated" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        outputTextField.setText(System.getProperty("user.home"));
    }

    /**
     * Loads data corresponding to events tab
     */
    private void loadEventsTab(){
        Iterable<Event> events;
        events = service.getAllEvents();

        List<Event> listEventReg = new ArrayList<>();
        List<Event> listEventUnreg = new ArrayList<>();

        events.forEach(x->{
                    if(!x.getOwner().getId().equals(currentId)) {
                        if (x.getParticipants() != null) {
                            int flag = 0;
                            for (User u : x.getParticipants()) {
                                if (u.getId().equals(currentId)) {
                                    listEventReg.add(x);
                                    flag = 1;
                                    break;
                                }
                            }

                            if (flag == 0) {
                                listEventUnreg.add(x);
                            }
                        } else {
                            listEventUnreg.add(x);
                        }
                    }
                });

        modelRegistered.setAll(listEventReg);
        modelUnregistered.setAll(listEventUnreg);
    }


    /**
     * Loads data corresponding to messages tab
     */
    private void loadMessagesTab(){
        Iterable<Message> messages;
        messages = service.getMesOnPage(0, currentId);

        List<Message> messageList = StreamSupport.stream(messages.spliterator(), false)
                .collect(Collectors.toList());

        modelMes.setAll(messageList);

        Iterable<Message> messagesSent;
        messagesSent = service.getMesOnPageSent(0, currentId);

        List<Message> messageListSent = StreamSupport.stream(messagesSent.spliterator(), false)
                .collect(Collectors.toList());

        modelMesSent.setAll(messageListSent);


        tableViewMain.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Loads data corresponding to friend request tab
     */
    private void loadRequestTab(){
        loadCommon(modelReq, modelNon);
    }

    /**
     * Loads common part of some tabs(friends, requests, users)
     * @param modelReq model of requests
     * @param modelNon model of users non friends
     */
    private void loadCommon(ObservableList<FriendRequestDTO> modelReq, ObservableList<User> modelNon) {
        HashSet<User> users;

        users = (HashSet<User>) service.getFriendsOnPage(0, currentId);
        List<User> userList = new ArrayList<>(users);

        modelMain.setAll(userList);

        Iterable<FriendRequest> requests;
        requests = service.getReqOnPage(0, currentId);

        ArrayList<FriendRequestDTO> toAdd = filterRequests(requests);

        modelReq.setAll(toAdd);

        HashSet<User> usersNon;

        usersNon = (HashSet<User>) service.getNonFriendsOnPage(0, currentId);
        List<User> userListNon = new ArrayList<>(usersNon);

        modelNon.setAll(userListNon);
    }

    /**
     * Filters requests and puts them into DTO
     * If current user is sender, first name and last name are of receiver
     * Else, first name and last name are of sender
     * @param requests all requests in which current user is involved
     * @return ArrayList of FriendRequestDTO
     */
    private ArrayList<FriendRequestDTO> filterRequests(Iterable<FriendRequest> requests) {
        ArrayList<FriendRequestDTO> toAdd = new ArrayList<>();
        Iterable<User> users = service.getAllUsers();
        Map<Long, User> userMap = StreamSupport.stream(users.spliterator(), false)
                                    .collect(Collectors.toMap(User::getId, Function.identity()));

        requests.forEach(x->{
            Tuple<Long, Long> tuple = new Tuple<>(x.getRequesterID(), x.getReceiverID());

            FriendRequestDTO requestDTO;
            String firstName;
            String lastName;
            if(x.getReceiverID().equals(currentId)) {
                firstName = userMap.get(x.getRequesterID()).getFirstName();
                lastName = userMap.get(x.getRequesterID()).getLastName();
            }
            else {
                firstName = userMap.get(x.getReceiverID()).getFirstName();
                lastName = userMap.get(x.getReceiverID()).getLastName();
            }
            requestDTO = new FriendRequestDTO(firstName
                    , lastName, x.getDate(), x.getStatus(), tuple);
            toAdd.add(requestDTO);
        });
        return toAdd;
    }

    /**
     * Loads data corresponding to MyPage tab
     */
    private void loadMyPageTab(){
        lastLabel.setText(service.getUser(currentId).getLastName());
        lastLabel.setStyle("-fx-font-size: 20px");
        firstLabel.setText(service.getUser(currentId).getFirstName());
        firstLabel.setStyle("-fx-font-size: 20px");

        //load all users not friends with current
        loadCommon(modelReqPage, modelNonPage);

        Iterable<Message> messages;
        messages = service.getMesOnPage(0, currentId);

        List<Message> messageList = StreamSupport.stream(messages.spliterator(), false)
                .collect(Collectors.toList());

        modelMesPage.setAll(messageList);

        tableViewMain.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * Initialises parameters and adds current observer to the service
     * @param service service of application
     */
    public void setService(Service service) {
        this.service = service;
        service.addObserver(this);
        initModel();

        Iterable<Event> events = service.getAllEvents();

        StringBuilder allString = new StringBuilder();
        ArrayList<String> evs = new ArrayList<>();
        events.forEach(x->{
            LocalDateTime now = LocalDateTime.now();

            if(x.getParticipants() != null){
                for(User u: x.getParticipants()){
                    if(u.getId().equals(currentId)){
                        long diff = ChronoUnit.DAYS.between(now, x.getDate());
                        if(diff == 1) {
                            evs.add(x.getName());
                        }
                    }
                }
            }
        });

        for(String s: evs){
            allString.append(s);
            allString.append("\n");
        }

        String finalAllString = allString.toString();

        if(!finalAllString.equals("")) {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() -> MessageAlert.showMessage(null,
                            Alert.AlertType.INFORMATION,
                            "Event info",
                            "Upcoming event: " + finalAllString));
                }
            };

            new Timer().schedule(task, 1000, Long.MAX_VALUE);
        }
    }

    /**
     * Sets session label and binds logout button to action
     * If logout button is clicked, login manager is notified and closes main window, then opening login window
     * @param loginManager login manager of app
     * @param sessionID id of logged in user
     */
    public void initSessionID(final LoginManager loginManager, Long sessionID) {
        sessionLabel.setText("Session ID: " + sessionID.toString());
        this.currentId = sessionID;
        logoutButton.setOnAction(event -> {
            try {
                loginManager.logout();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Handles on click send request button
     * Sends friend request to user with input first name and last name
     * If there are multiple users with given name, new window is loaded containing all users with given name
     */
    public void handleSendRequest(){
        User usr;
        if(tabPane.getSelectionModel().getSelectedItem().equals(friendRequestTab)) {
            usr = tableViewNonFriends.getSelectionModel().getSelectedItem();
        }
        else {
            usr = tableViewNonPage.getSelectionModel().getSelectedItem();
        }

        if(usr == null){
            MessageAlert.showErrorMessage(null, "Select user to send friendship!");
        }
        else{
            try {
                service.addFriendRequestID(usr.getId(), currentId);
                searchBox.clear();
            }
            catch (ServiceException | ValidationException e){
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }

    /**
     * Handles on click remove friend button
     * Removes selected friend from list alongside the corresponding friend request
     */
    public void handleRemove(){
        User usr = tableViewMain.getSelectionModel().getSelectedItem();
        if(usr != null){
            try {
                service.removeFriendshipID(usr.getId().toString(), currentId.toString());
                service.removeFriendRequestID(usr.getId(), currentId);
            }
            catch (ValidationException e){
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Select user to remove friendship!");
        }
    }

    /**
     * Handle for selection changed of a tab
     */
    public void handleSelectionChanged(){
        if(service != null) {
            initModel();
        }
    }

    /**
     * Handles on click accept button
     * Accepts selected friend request if current user is receiver of request
     */
    public void handleAccept(){
        FriendRequestDTO req;
        if (tabPane.getSelectionModel().getSelectedItem().equals(friendRequestTab)) {
            req = tableViewReq.getSelectionModel().getSelectedItem();
        }
        else {
            req = tableViewReqPage.getSelectionModel().getSelectedItem();
        }

        if(req != null){
            try {
                if(req.getId().getRight().equals(currentId)) {
                    service.acceptFriendRequest(service.getFriendRequest(req.getId()));
                    service.addFriendShipID(req.getId().getLeft().toString(), req.getId().getRight().toString());
                }
            }
            catch (ValidationException e){
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Select friend request!");
        }
    }

    /**
     * Handles on click deny button
     * Denies selected friend request if current user is receiver of request
     */
    public void handleDeny(){
        FriendRequestDTO req;
        if(tabPane.getSelectionModel().getSelectedItem().equals(friendRequestTab)) {
            req = tableViewReq.getSelectionModel().getSelectedItem();
        }
        else {
            req = tableViewReqPage.getSelectionModel().getSelectedItem();
        }

        if(req != null){
            try {
                if(req.getId().getRight().equals(currentId)) {
                    service.denyFriendRequest(service.getFriendRequest(req.getId()));
                }
            }
            catch (ValidationException e){
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Select friend request!");
        }
    }

    /**
     * Handles on click withdraw request button
     * Deletes friend request if it is not answered and current user is the sender
     */
    public void handleWithdraw(){
        FriendRequestDTO req;
        if(tabPane.getSelectionModel().getSelectedItem().equals(friendRequestTab)) {
            req = tableViewReq.getSelectionModel().getSelectedItem();
        }
        else {
            req = tableViewReqPage.getSelectionModel().getSelectedItem();
        }

        if(req != null){
            try {
                if(req.getId().getLeft().equals(currentId) && req.getStatus().equals("Pending")){
                    service.removeFriendRequestID(req.getId().getLeft(), req.getId().getRight());
                }
                else if (!req.getStatus().equals("Pending")){
                    MessageAlert.showErrorMessage(null, "Request must be pending in order to withdraw!");
                }
                else {
                    MessageAlert.showErrorMessage(null, "You must be sender in order to withdraw request!");
                }
            }
            catch (ValidationException e){
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Select friend request!");
        }
    }

    /**
     * Handles on click send button
     * Sends message from text area to selected user from friends table OR
     * Replies message selected from message list
     */
    public void handleSend(){
        List<User> users = tableViewMain.getSelectionModel().getSelectedItems();
        TextArea textArea;
        if(tabPane.getSelectionModel().getSelectedItem().equals(messagesTab)){
            textArea = textAreaMes;
        }
        else {
            textArea = textAreaPage;
        }

        if(!users.isEmpty() && !textArea.getText().isEmpty()){
            try {
                List<Long> ids = new ArrayList<>();
                users.forEach(x-> ids.add(x.getId()));
                service.sendMessageID(ids, currentId, textArea.getText());
                textArea.clear();
            }
            catch (ValidationException e){
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }

        Message mes;
        if(tabPane.getSelectionModel().getSelectedItem().equals(messagesTab)) {
            mes = tableViewMes.getSelectionModel().getSelectedItem();
        }
        else {
            mes = tableViewMesPage.getSelectionModel().getSelectedItem();
        }
        if(mes != null && !textArea.getText().isEmpty()){
            try {
                List<Long> ids = new ArrayList<>();
                ids.add(mes.getFrom().getId());

                service.sendReply(ids, currentId, textArea.getText(), mes);
                textArea.clear();
            }
            catch (ValidationException e){
                MessageAlert.showErrorMessage(null, e.getMessage());
            }
        }
    }

    /**
     * Handles on click browse button
     * Opens default file explorer for OS and inputs inside text field absolute path to directory
     */
    public void handleBrowse(){
        DirectoryChooser fileChooser = new DirectoryChooser();

        File file = fileChooser.showDialog(null);

        if(file != null){
            outputTextField.clear();
            outputTextField.appendText(file.getAbsolutePath());
        }
    }

    /**
     * Handles on click generate button
     * Generates report based on user input
     * @throws IOException if IOException occurs while creating PDF document
     */
    public void handleGenerate() throws IOException {
        if(startDate.getValue() == null || endDate.getValue() == null){
            MessageAlert.showErrorMessage(null, "Select date in order to generate report!");
            return;
        }

        LocalDateTime start = startDate.getValue().atStartOfDay();
        LocalDateTime end = endDate.getValue().atStartOfDay();

        if(!checkBoxReport1.isSelected() && !checkBoxReport2.isSelected()){
            MessageAlert.showErrorMessage(null, "Select checkbox in order to generate report!");
            return;
        }

        if(outputTextField.getText().isEmpty()){
            MessageAlert.showErrorMessage(null, "Select output folder!");
            return;
        }

        if(nameReport.getText().isEmpty()){
            MessageAlert.showErrorMessage(null, "Select name of report!");
            return;
        }
        List<Message> messages1 = service.getMessagesPeriod(currentId, start, end);
        List<Friendship> friendships = service.getFriendshipsPeriod(currentId, start, end);

        ReportGenerator generator = new ReportGenerator(nameReport.getText(), outputTextField.getText(), service);

        if(checkBoxReport1.isSelected()) {
            //report 1 new friends and message in given period
            modelMesRep.clear();
            modelFriendRep.clear();

            generator.generateReport1(messages1, friendships, currentId);
            modelMesRep.setAll(messages1);

            List<FriendRequestDTO> friendReq = new ArrayList<>();

            friendships.forEach(x->{
                FriendRequestDTO fr;
                if(x.getId().getLeft().equals(currentId)){
                    fr = new FriendRequestDTO(service.getUser(x.getId().getRight()).getFirstName(),
                            service.getUser(x.getId().getRight()).getLastName(), x.getDate(), "foo", x.getId());
                }
                else {
                    fr = new FriendRequestDTO(service.getUser(x.getId().getLeft()).getFirstName(),
                            service.getUser(x.getId().getLeft()).getLastName(), x.getDate(), "foo", x.getId());
                }
                friendReq.add(fr);
            });

            modelFriendRep.setAll(friendReq);
        }
        else {
            //report 2
            User usr = tableViewMain.getSelectionModel().getSelectedItem();
            if(usr == null){
                MessageAlert.showErrorMessage(null, "Select user in order to generate report 2!");
                return;
            }
            modelMesRep.clear();
            modelFriendRep.clear();

            List<Message> messages2 = service.getMessagesUserPeriod(currentId, usr.getId(), start, end);
            modelMesRep.setAll(messages2);
            generator.generateReport2(messages2, usr);
        }
        nameReport.clear();
        outputTextField.clear();
        checkBoxReport1.setSelected(false);
        checkBoxReport2.setSelected(false);

        startDate.setValue(null);
        endDate.setValue(null);

        MessageAlert.showMessage(null, Alert.AlertType.CONFIRMATION, "Info", "Report generated.");
    }

    /**
     * Filters users displayed to match with input text in search box
     * @param users list of users
     * @return list of users but filtered
     */
    private ObservableList<User> filteredUsers(List<User> users, TextField field){
        List<User> filteredList = new ArrayList<>();
        users.forEach(x->{
                if (criteria(x, field)) {
                    filteredList.add(x);
                }
        });
        return FXCollections.observableList(filteredList);
    }

    /**
     * Checks if user matches with input text in search box
     * @param user user to be checked
     * @return true if user matches, false otherwise
     */
    private boolean criteria(User user, TextField field){
        String text = field.getText();
        if(text == null || text.isEmpty()){
            return true;
        }

        String lowerCaseText = text.toLowerCase();

        return user.getLastName().toLowerCase().contains(lowerCaseText) ||
                user.getFirstName().toLowerCase().contains(lowerCaseText) ||
                (user.getFirstName().toLowerCase() + " " + user.getLastName().toLowerCase()).contains(lowerCaseText) ||
                (user.getLastName().toLowerCase() + " " + user.getFirstName().toLowerCase()).contains(lowerCaseText);
    }

    /**
     * Handles on click add event button
     * Adds new event
     */
    public void handleAddEvent(){
        String name = eventName.getText();
        String desc = eventDesc.getText();

        LocalDateTime date = dateEvent.getValue().atTime(spinner.getValue());

        if(!name.isEmpty() && !desc.isEmpty()) {
            service.addEvent(name, desc, currentId, date);
        }

        eventDesc.clear();
        eventName.clear();
        dateEvent.setValue(null);
        setSpinnerFactory();
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Confirmation", "Event added!");
    }

    /**
     * Handles on click follow event button
     * Current user will now follow selected event
     */
    public void handleFollow(){
        Event event = tableViewUnregistered.getSelectionModel().getSelectedItem();

        if(event != null){
            service.addParticipant(currentId, event.getId());
            modelParticipants.clear();
        }
    }

    /**
     * Handles on click unfollow event button
     * Current user will not follow selected event anymore
     */
    public void handleUnfollow(){
        Event event = tableViewRegistered.getSelectionModel().getSelectedItem();

        if(event != null){
            service.removeParticipant(currentId, event.getId());
            modelParticipants.clear();
        }
    }
}
