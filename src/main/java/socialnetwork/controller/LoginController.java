/*
 *  @author albua
 *  created on 19/11/2020
 */
package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.User;

import java.io.IOException;
import java.util.Optional;

import javafx.scene.control.TextField;
import socialnetwork.repository.database.UserDatabase;
import socialnetwork.service.Service;

/**
 * Controller class for login screen
 */
public class LoginController {
    UserDatabase userRepo;
    Service service;

    @FXML
    TextField emailTextField;
    @FXML
    TextField passwordTextField;
    @FXML
    Button loginButton;

    @FXML
    public void initialize(){}

    /**
     * Bind login button to action
     * Call login manager and authenticate user if credentials are right
     * @param loginManager login manager
     */
    public void initManager(final LoginManager loginManager){
        loginButton.setOnAction(event -> {
            String email = emailTextField.getText();
            String passwd = passwordTextField.getText();

            Optional<User> usr = userRepo.getByEmailPass(email, passwd);

            if(!usr.isPresent()) {
                MessageAlert.showErrorMessage(null, "Invalid credentials");
            }
            else{
                try {
                    //tell login manager to open main window for this user and close login window
                    loginManager.authenticated(usr.get().getId());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Sets parameters for login controller
     * @param userRepo database repository for users
     * @param service service of application
     */
    public void setService(UserDatabase userRepo, Service service){
        this.userRepo = userRepo;
        this.service = service;
    }

    /**
     * Handle on click register button
     * Loads up register window
     */
    public void handleRegister(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/registerUserView.fxml"));

            AnchorPane root = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Register user");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            RegisterUserController registerUserController = loader.getController();
            registerUserController.setService(service, dialogStage);
            dialogStage.show();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
