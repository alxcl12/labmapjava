/*
 *  @author albua
 *  created on 22/11/2020
 */
package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.Service;

/**
 * Controller class for register user window
 * Window is displayed when users wants to register to application
 */
public class RegisterUserController {
    Service service;
    Stage dialogStage;

    @FXML
    TextField textFieldFirst;
    @FXML
    TextField textFieldLast;
    @FXML
    TextField textFieldEmail;
    @FXML
    TextField textFieldPassword;

    /**
     * Initialises parameters
     * @param serv service of application
     * @param dialogStage current stage
     */
    public void setService(Service serv, Stage dialogStage){
        this.service = serv;
        this.dialogStage = dialogStage;
    }

    /**
     * Handles on click cancel button
     * Closes the window
     */
    public void handleCancel(){
        dialogStage.close();
    }

    /**
     * Handles on click register button
     * Adds user to application if inputs are ok
     * Displays error window if they are not
     */
    public void handleRegister(){
        String firstName = textFieldFirst.getText();
        String lastName = textFieldLast.getText();
        String email = textFieldEmail.getText();
        String password = textFieldPassword.getText();

        try{
            service.addUser(firstName, lastName, email, password);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Success!", "User registered successfully!");
            dialogStage.close();
        }
        catch (ValidationException | ServiceException e){
            MessageAlert.showErrorMessage(null, e.getMessage());
        }
    }
}
